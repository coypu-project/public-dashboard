import pandas as pd
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, XSD

def load_table():
    df = pd.read_csv('../source_data/VGR-81000-0009.csv', encoding='cp1250', delimiter=';', index_col=False, decimal=',')
    return df

def create_graph(graph):
    base_iri = 'https://data.coypu.org/observation/81000-0009/'
    table = load_table()

    for index, row in table.iterrows():
        iri = URIRef(base_iri + str(row['Zeit']) + '/')
        graph.add((iri, RDF.type, qb.Observation))
        graph.add((iri, qb.dataSet, URIRef('https://data.coypu.org/datasets/genesis/81000-0009')))
        graph.add((iri, cq.etg004, Literal(row['ETG004_____Arbeitnehmerentgelt_(Inlaender)__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.soz002, Literal(row['SOZ002___-_Sozialbeitraege_der_Arbeitgeber__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.vst006, Literal(row['VST006___=_Bruttoloehne_und_-gehaelter_(Inlaender)__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.abz002, Literal(row['ABZ002___-_Abzuege_der_Arbeitnehmer__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.vst013, Literal(row['VST013___=_Nettoloehne_u._-gehaelter_(Inlaender)__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.lst005, Literal(row['LST005___+_Monetaere_Sozialleistungen__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.abg0003, Literal(row['ABG003___-_Abgaben_auf_soz._Leist.,_verbrauchsnahe_Steuern__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.ekm003, Literal(row['EKM003___=_Masseneinkommen_(private_Haushalte)__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.vgr032, Literal(row['VGR032___+_Betriebsuebersch./Selbstaend.-,Verm.eink._priv.HH__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.trf005, Literal(row['TRF005___+_Weitere_empfangene_abzgl._geleistete_Transfers__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.ekm006, Literal(row['EKM006___=_Verfuegbares_Einkommen_der_privaten_Haushalte__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.ekm012, Literal(row['EKM012___nachrichtl.:_Verfuegb._Einkommen_(Verbrauchskzpt.)__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.vgr091, Literal(row['VGR091________________Zunahme_betriebl._Versorg.ansprueche__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.vgr037, Literal(row['VGR037________________Sparen_der_privaten_Haushalte__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.vgr092, Literal(row['VGR092________________Bezugsgroesse_fuer_die_Sparquote__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.vgr093, Literal(row['VGR093________________Sparquote_der_privaten_Haushalte__Prozent'], datatype=XSD.decimal)))
        graph.add((iri, coy.hasYear, Literal(row['Zeit'], datatype=XSD.gYear)))

if __name__ == '__main__':
    graph = Graph()

    coy = Namespace('https://schema.coypu.org/global#')
    graph.bind('coy', coy)

    cq = Namespace('https://schema.coypu.org/coy-cube#')
    graph.bind('cq', cq)

    qb = Namespace('http://purl.org/linked-data/cube#')
    graph.bind('qb', qb)
 
    create_graph(graph)
    graph.serialize(destination='../transformed_data/VGR-81000-0009.ttl', format="turtle")
