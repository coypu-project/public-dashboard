import pandas as pd
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, XSD

genesis_iri = URIRef('https://data.coypu.org/classification/genesis-code/')

def get_codes(code):
    if not code:
        return []

    code = code.replace(" ","")
    parts = code.split()
    codeList = []

    for part in parts:
        if 'bis' in part and len(part) > 2:
            start, sep, end = part.partition('bis')
            if len(start) == 1 and len(end) == 1:
                codeList.extend([chr(ord(start) + i) for i in range(ord(end) - ord(start) + 1)])
            else:
                codeList.append(part)
        elif '-' in part and len(part) > 1:
            start, end = map(int, part.split('-'))
            codeList.extend(map(str, range(start, end + 1)))
        else:
            codeList.append(part)

    return codeList

def load_table():
    df = pd.read_csv('../source_data/VGR-81000-0110.csv', encoding='cp1250', delimiter=';', index_col=False, decimal=',')
    return df

def load_mapping():
    df = pd.read_csv('../source_data/wz-nace-map.csv', encoding='cp1250', delimiter=';', index_col=False, header=1)
    return df

def create_graph(graph):
    base_iri = 'https://data.coypu.org/observation/81000-0110/'
    table = load_table()
    map = load_mapping()

    for index, row in table.iterrows():
        iri = URIRef(base_iri + str(row['Zeit']) + '/' + str(row['1_Auspraegung_Code']) + '/' + str(row['2_Auspraegung_Code']) + '/')
        tradeCode = row['2_Auspraegung_Code']      
        graph.add((iri, RDF.type, qb.Observation))
        graph.add((iri, qb.dataSet, URIRef('https://data.coypu.org/datasets/genesis/81000-0110')))
        graph.add((iri, coy.country, Literal('https://data.coypu.org/country/DEU')))
        graph.add((iri, coy.hasYear, Literal(row['Zeit'], datatype=XSD.gYear)))
        graph.add((iri, cq.tradeGroup, Literal(genesis_iri + tradeCode)))
        v = row['ETG003__Arbeitnehmerentgelt__Mrd._EUR'].replace(',', '.')
        if v == '...':
            graph.add((iri, cq.etg003, Literal('empty')))
        else:
            graph.add((iri, cq.etg003, Literal(v, datatype=XSD.decimal)))
        
if __name__ == '__main__':
    graph = Graph()

    coy = Namespace('https://schema.coypu.org/global#')
    graph.bind('coy', coy)

    cq = Namespace('https://schema.coypu.org/coy-cube#')
    graph.bind('cq', cq)      

    qb = Namespace('http://purl.org/linked-data/cube#')
    graph.bind('qb', qb)

    create_graph(graph)
    graph.serialize(destination='../transformed_data/VGR-81000-0110.ttl', format="turtle")
