import pandas as pd
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, XSD

def load_table():
    df = pd.read_csv('../source_data/VGR-81000-0130.csv', encoding='cp1250', delimiter=';', index_col=False, decimal=',')
    return df

def create_graph(graph):
    base_iri = 'https://data.coypu.org/observation/81000-0130/'
    table = load_table()

    for index, row in table.iterrows():
        iri = URIRef(base_iri + str(row['Zeit']) + '/' + str(row['2_Auspraegung_Code']) + '/')
        graph.add((iri, RDF.type, qb.Observation))
        graph.add((iri, qb.dataSet, URIRef('https://data.coypu.org/datasets/genesis/81000-0130')))
        graph.add((iri, cq.enm005, Literal(row['ENM005__Einnahmen_und_Ausgaben_Uebrige_Welt__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.typeOfIncomeAndExpensesCode, Literal(row['2_Auspraegung_Code'])))
        graph.add((iri, cq.typeOfIncomeAndExpenses, Literal(row['2_Auspraegung_Label'])))
        graph.add((iri, coy.hasYear, Literal(row['Zeit'], datatype=XSD.gYear)))

if __name__ == '__main__':
    graph = Graph()

    coy = Namespace('https://schema.coypu.org/global#')
    graph.bind('coy', coy)

    cq = Namespace('https://schema.coypu.org/coy-cube#')
    graph.bind('cq', cq)      

    qb = Namespace('http://purl.org/linked-data/cube#')
    graph.bind('qb', qb)

    create_graph(graph)
    graph.serialize(destination='../transformed_data/VGR-81000-0130.ttl', format="turtle")
