import pandas as pd
import json
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, XSD

nace_iri = URIRef('https://data.coypu.org/classification/nace_r2/')

def load_table():
    df = pd.read_csv('../source_data/Außenhandel_51000-0036.csv', encoding='cp1250', delimiter=';', index_col=False, decimal=',')
    return df

def create_import_graph(graph):
    base_iri = 'https://data.coypu.org/observation/51000-0036/import/'
    table = load_table()
    with open('../source_data/mapping.json', 'r') as json_file:
        all_mappings = json.load(json_file)
        country_mapping = all_mappings["country_mapping"]

    for index, row in table.iterrows():
        iri = URIRef(base_iri + str(row['Zeit']) + '/' + str(row['1_Auspraegung_Code']) + '/' + str(row['2_Auspraegung_Code'])) + '/' + str(row['3_Auspraegung_Code'])
        state_iri = URIRef('https://data.coypu.org/geoboundaries/DEU/1/' + row['2_Auspraegung_Label'])
        country_code = row['1_Auspraegung_Code']
        country_label = country_mapping.get(country_code, '')
        if (row['WERTE__Einfuhr:_Wert__Tsd._EUR'] != '-' and row['GEWE__Einfuhr:_Gewicht__t'] != '-'):
            graph.add((iri, RDF.type, qb.Observation))
            graph.add((iri, cq.state, state_iri))
            graph.add((iri, qb.dataSet, URIRef('https://data.coypu.org/datasets/genesis/51000-0036')))
            graph.add((iri, cq.tradeDirection, URIRef('https://data.coypu.org/tradeDirection/import')))
            graph.add((iri, cq.tradeCountry, URIRef(country_label)))
            graph.add((iri, cq.tradeGroup, URIRef(nace_iri + (row['3_Auspraegung_Code'][-2:]))))
            graph.add((iri, coy.hasYear, Literal(row['Zeit'], datatype=XSD.gYear)))
            graph.add((iri, cq.value, Literal(row['WERTE__Einfuhr:_Wert__Tsd._EUR'].replace(',', '.'), datatype=XSD.decimal)))
            graph.add((iri, cq.weight, Literal(row['GEWE__Einfuhr:_Gewicht__t'].replace(',', '.'), datatype=XSD.decimal)))            
            
def create_export_graph(graph):
    base_iri = 'https://data.coypu.org/observation/51000-0036/export/'
    table = load_table()
    with open('../source_data/mapping.json', 'r') as json_file:
        all_mappings = json.load(json_file)
        country_mapping = all_mappings["country_mapping"]

    for index, row in table.iterrows():
        iri = URIRef(base_iri + str(row['Zeit']) + '/' + str(row['1_Auspraegung_Code']) + '/' + str(row['2_Auspraegung_Code'])) + '/' + str(row['3_Auspraegung_Code'])
        state_iri = URIRef('https://data.coypu.org/geoboundaries/DEU/1/' + row['2_Auspraegung_Label'])
        country_code = row['1_Auspraegung_Code']
        country_label = country_mapping.get(country_code, '')
        if (row['WERTA__Ausfuhr:_Wert__Tsd._EUR'] == '-' and row['GEWA__Ausfuhr:_Gewicht__t'] == '-') == False:
            graph.add((iri, RDF.type, qb.Observation))
            graph.add((iri, cq.state, state_iri))
            graph.add((iri, qb.dataSet, URIRef('https://data.coypu.org/datasets/genesis/51000-0036')))
            graph.add((iri, cq.tradeDirection, URIRef('https://data.coypu.org/tradeDirection/export')))
            graph.add((iri, cq.tradeCountry, URIRef(country_label)))
            graph.add((iri, cq.tradeGroup, URIRef(nace_iri + (row['3_Auspraegung_Code'][-2:]))))
            graph.add((iri, coy.hasYear, Literal(row['Zeit'], datatype=XSD.gYear)))
            graph.add((iri, cq.value, Literal(row['WERTA__Ausfuhr:_Wert__Tsd._EUR'].replace(',', '.'), datatype=XSD.decimal)))
            graph.add((iri, cq.weight, Literal(row['GEWA__Ausfuhr:_Gewicht__t'].replace(',', '.'), datatype=XSD.decimal)))                


if __name__ == '__main__':
    graph = Graph()

    coy = Namespace('https://schema.coypu.org/global#')
    graph.bind('coy', coy)
    
    cq = Namespace('https://schema.coypu.org/coy-cube#')
    graph.bind('cq', cq)

    qb = Namespace('http://purl.org/linked-data/cube#')
    graph.bind('qb', qb)

    create_import_graph(graph)
    create_export_graph(graph)
    graph.serialize(destination='../transformed_data/außenhandelLaender_51000-0036.ttl', format="turtle")