import pandas as pd
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, XSD

def load_table():
    df = pd.read_csv('../source_data/VGR-82411-0001.csv', encoding='cp1250', delimiter=';', index_col=False, decimal=',')
    return df

def create_graph(graph):
    base_iri = 'https://data.coypu.org/observation/82411-0001/'
    table = load_table()
    for index, row in table.iterrows():
        iri = URIRef(base_iri + str(row['Zeit']) + '/' + str(row['1_Auspraegung_Code']) + '/')
        state_iri = URIRef('https://data.coypu.org/state/' + row['1_Auspraegung_Label'])
        graph.add((iri, RDF.type, qb.Observation))
        if row['1_Auspraegung_Label'] != "Deutschland":
            graph.add((iri, cq.state, state_iri))
        else:
            graph.add((iri, coy.country, URIRef('https://data.coypu.org/country/DEU')))         
        graph.add((iri, qb.dataSet, URIRef('https://data.coypu.org/datasets/genesis/82411-0001')))
        graph.add((iri, coy.hasYear, Literal(row['Zeit'], datatype=XSD.gYear)))
        graph.add((iri, cq.ekm006, Literal(row['EKM006__Verfuegb.Einkommen_d.priv.Haushalte_(Ausgabenkzpt)__Mill._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.ekm014, Literal(row['EKM014__Verfuegb._Einkommen_d._priv._Haushalte_je_Einwohner__EUR'], datatype=XSD.decimal)))

if __name__ == '__main__':
    graph = Graph()

    coy = Namespace('https://schema.coypu.org/global#')
    graph.bind('coy', coy)

    cq = Namespace('https://schema.coypu.org/coy-cube#')
    graph.bind('cq', cq) 

    qb = Namespace('http://purl.org/linked-data/cube#')
    graph.bind('qb', qb)

    create_graph(graph)
    graph.serialize(destination='../transformed_data/VGR-82411-0001.ttl', format="turtle")
