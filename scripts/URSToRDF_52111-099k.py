import pandas as pd
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, XSD

genesis_iri = URIRef('https://data.coypu.org/classification/genesis-code/')

def load_table():
    df = pd.read_csv('../source_data/sachsen_52111-099.csv', encoding='cp1250', delimiter=';', index_col=False, decimal=',')
    return df

def create_graph(graph):
    base_iri = 'https://data.coypu.org/observation/52111-099/'
    table = load_table()

    for index, row in table.iterrows():
        iri = URIRef(base_iri + str(row['Zeit'][-4:]) + '/' + str(row['1_Auspraegung_Code']) + '/' + str(row['2_Auspraegung_Code']) + '/')
        graph.add((iri, RDF.type, qb.Observation))
        graph.add((iri, qb.dataSet, URIRef('https://data.coypu.org/datasets/genesis/52111-099')))
        graph.add((iri, cq.evaluationDate, Literal(row['Zeit'], datatype=XSD.date)))
        graph.add((iri, cq.district, Literal(row['1_Auspraegung_Label']))) 
        # graph.add((iri, cq.districtCode, Literal(row['1_Auspraegung_Code']))) 
        graph.add((iri, cq.tradeGroup, URIRef(genesis_iri + str(row['2_Auspraegung_Code']))))
        graph.add((iri, cq.UNT, Literal(row['UNT513__Rechtliche_Einheiten__(B-N,P-S)__Anzahl'])))

if __name__ == '__main__':
    graph = Graph()

    coy = Namespace('https://schema.coypu.org/global#')
    graph.bind('coy', coy)

    cq = Namespace('https://schema.coypu.org/coy-cube#')
    graph.bind('cq', cq)

    qb = Namespace('http://purl.org/linked-data/cube#')
    graph.bind('qb', qb)
 
    create_graph(graph)
    graph.serialize(destination='../transformed_data/URS-52111-099k.ttl', format="turtle")
