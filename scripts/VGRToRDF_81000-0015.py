import pandas as pd
import numpy as np
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, XSD

genesis_iri = URIRef('https://data.coypu.org/classification/genesis-code/')

def load_table():
    df = pd.read_csv('../source_data/VGR-81000-0015.csv', encoding='cp1250', delimiter=';', index_col=False, decimal=',')
    return df

def create_graph(graph):
    base_iri = 'https://data.coypu.org/observation/81000-0015/'
    table = load_table()

    for index, row in table.iterrows():
        tradeCode = row['2_Auspraegung_Code']
        iri = URIRef(base_iri + str(row['Zeit']) + '/' + str(row['2_Auspraegung_Code']) + '/')
        graph.add((iri, RDF.type, qb.Observation))
        graph.add((iri, qb.dataSet, URIRef('https://data.coypu.org/datasets/genesis/81000-0015'))) 
        graph.add((iri, cq.etg005, Literal(row['ETG005__Arbeitnehmerentgelt_(im_Inland)__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.vst020, Literal(row['VST020__Bruttoloehne_und_-gehaelter_(im_Inland)__Mrd._EUR'], datatype=XSD.decimal)))
        graph.add((iri, cq.erw063, Literal(row['ERW063__Erwerbstaetige_(im_Inland)__1000'], datatype=XSD.int)))
        graph.add((iri, cq.erw061, Literal(row['ERW061__Arbeitnehmer_(im_Inland)__1000'], datatype=XSD.int)))
        graph.add((iri, cq.std002, Literal(row['STD002__Arbeitsstunden_der_Erwerbstaetigen_(im_Inland)__Mill._Std.'], datatype=XSD.decimal)))
        graph.add((iri, cq.std003, Literal(row['STD003__Arbeitsstunden_der_Arbeitnehmer_(im_Inland)__Mill._Std.'], datatype=XSD.decimal)))
        graph.add((iri, cq.std007, Literal(row['STD007__Arbeitsstunden_je_Erwerbstaetigen_(im_Inland)__Std.'], datatype=XSD.decimal)))
        graph.add((iri, cq.std008, Literal(row['STD008__Arbeitsstunden_je_Arbeitnehmer_(im_Inland)__Std.'], datatype=XSD.decimal)))            
        graph.add((iri, coy.hasYear, Literal(row['Zeit'], datatype=XSD.gYear)))
        if pd.isna(tradeCode) == False:
            graph.add((iri, cq.tradeGroup, URIRef(genesis_iri + str(tradeCode))))

if __name__ == '__main__':
    graph = Graph()

    coy = Namespace('https://schema.coypu.org/global#')
    graph.bind('coy', coy)

    cq = Namespace('https://schema.coypu.org/coy-cube#')
    graph.bind('cq', cq)

    qb = Namespace('http://purl.org/linked-data/cube#')
    graph.bind('qb', qb)

    create_graph(graph)
    graph.serialize(destination='../transformed_data/VGR-81000-0015.ttl', format="turtle")
