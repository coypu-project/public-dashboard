#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# CoyPu Public Dashboard 
# Version 17 - 08.02.2024
# Includes: All available plots 
# In progress 1: Polygon area is drawn on the map depending on the polygon area coordinate 
# In progress 2: Dynamic menu creation and use selected event in query dynamically depending on the Event type selection on dynamic menu. Example: Disaster > Natural Disaster > GeophysicalDisaster > Earthquake etc.
# In progress 3: Binding multiple queries with respect to dynamic values. 
#              Example: Event type - Flood > Send query using Flood to list all flood events > 
#                       Display events on table > Select an event on table and query is sent depending on this selection >
#                       Get polygon area of the selected event > Use this polygon area in query to get effected countries and trade groups >
# Tasks in progress are partly done:
# 1 : completed, will be integrated to main code
# 2 : in progress
# 3 : partly completed, will be integrated to main code

import numpy as np
import panel as pn
import pandas as pd
import holoviews as hv
import hvplot.pandas
import datetime as dt
#from panel_highcharts import HighChart
import plotly.express as px
from bokeh.models import Button 

from datetime import date
from random import randint

from bokeh.io import show
from bokeh.models import ColumnDataSource, DataTable, DateFormatter, TableColumn

import sqlite3
import time
import datetime
import time, threading
from dateutil import parser

from bokeh.plotting import figure
from bokeh.io import curdoc
from bokeh.driving import linear

from bokeh.layouts import row, column, gridplot
from bokeh.models import CustomJS, MultiChoice, ColumnDataSource, Slider, Select, Tooltip
from bokeh.models import (ColumnDataSource, DataTable, NumberFormatter, RangeTool, StringFormatter, TableColumn)


from bokeh.plotting import curdoc, figure
from bokeh.driving import count
from bokeh.models.widgets import DataTable, DateFormatter, TableColumn, PreText, Button, TextInput
from bokeh.models.callbacks import CustomJS

import plotly.graph_objects as go

import random
import geopandas as gpd
from bokeh.io import show
from bokeh.plotting import figure
from bokeh.models import GeoJSONDataSource, ColorBar, HoverTool
from bokeh.transform import linear_cmap
from bokeh.palettes import Viridis256


# In[ ]:


# config variables
import os
#p_dir = 'C:\Users\FatihKilic\Desktop\jupyter_notebooks'
p_dir = os.path.abspath('.')
print(p_dir)


# In[ ]:


# ----------------------------------------DATA-------------------------------------------------------------------

pd.set_option('display.max_columns', None)

dataframes = []

def getDataFrame(file_name):
    df = pd.read_csv(file_name, encoding='latin-1', delimiter=';')
    return df


csv_file_names = [
    os.path.join(p_dir, 'data_all', '51000-0034-DLANDX_$F_flat.csv'),
    os.path.join(p_dir, 'data_all', '51000-0034_flat_2008_2022.csv'),
    os.path.join(p_dir, 'data_all', '51000-0036_flat_2018.csv'),
    os.path.join(p_dir, 'data_all', '51000-0036_flat_2019.csv'),
    os.path.join(p_dir, 'data_all', '51000-0036_flat_2020.csv'),
    os.path.join(p_dir, 'data_all', '51000-0036_flat_2021.csv'),
    os.path.join(p_dir, 'data_all', '51000-0036_flat_2022.csv'),   
    os.path.join(p_dir, 'data_all', 'en', '81000-0009_$F_flat_en.csv'),
    os.path.join(p_dir, 'data_all', 'en', '81000-0015_$F_flat_en.csv'),
    os.path.join(p_dir, 'data_all', 'en', '81000-0110_$F_flat_compensation_of_employees.csv'),
    os.path.join(p_dir, 'data_all', 'en', '81000-0110_flat_compensation_of_employees_per_employee.csv'),
    os.path.join(p_dir, 'data_all', 'en', '81000-0110_flat_compensation_of_employees_per_employee_hour.csv'),
    os.path.join(p_dir, 'data_all', 'en', '81000-0127_flat_en.csv'),
    os.path.join(p_dir, 'data_all', 'en', '81000-0130_flat_to_rest_of_the_world.csv'),
    os.path.join(p_dir, 'data_all', 'en', '81000-0130_flat_to_third_countries.csv'),
    os.path.join(p_dir, 'data_all', 'en', '81000-0130_flat_to_eu.csv'),
    os.path.join(p_dir, 'data_all', '82111-0001-DLAND_$F_flat.csv'),
    os.path.join(p_dir, 'data_all', 'en', '82111-0002-DLAND_$F_flat_en.csv'),
    os.path.join(p_dir, 'data_all', '82411-0001-DLAND_$F_flat.csv'),
]

for i, csv_file in enumerate(csv_file_names):
    data = getDataFrame(csv_file)
    dataframes.append(data)
    
for i, data in enumerate(dataframes):
    dataF0 = data.fillna(0)
    dataNaN = dataF0.replace("-", np.nan)
    dataClean = dataNaN.replace("...", np.nan)
    dataframes[i] = dataClean

dt0 = dataframes[0]
dt1 = dataframes[1]
dt2 = dataframes[2]
dt3 = dataframes[3]
dt4 = dataframes[4]
dt5 = dataframes[5]
dt6 = dataframes[6]
dt7 = dataframes[7]
dt8 = dataframes[8]
dt9 = dataframes[9]
dt10 = dataframes[10]
dt11 = dataframes[11]
dt12 = dataframes[12]
dt13 = dataframes[13]
dt14 = dataframes[14]
dt15 = dataframes[15]
dt16 = dataframes[16]
dt17 = dataframes[17]
dt18 = dataframes[18]


# In[ ]:


###----- 51000-0034: Exports and imports (foreign trade): LÃ¤nder, years, classifications of trading goods -----####
selected_columns = ["Zeit", 
                    "1_Auspraegung_Code", 
                    "1_Auspraegung_Label", 
                    "2_Auspraegung_Code", 
                    "2_Auspraegung_Label", 
                    "GEWA__Ausfuhr:_Gewicht__t",
                    "WERTA__Ausfuhr:_Wert__Tsd._EUR",
                    "GEWE__Einfuhr:_Gewicht__t",
                    "WERTE__Einfuhr:_Wert__Tsd._EUR"]
dt0_selected_columns = dt0[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {'Zeit': 'Year',
                  '1_Auspraegung_Code': 'State Code',
                  '1_Auspraegung_Label': 'State',
                  '2_Auspraegung_Code': 'Trade Code',
                  '2_Auspraegung_Label': 'Trade Group',
                  'GEWA__Ausfuhr:_Gewicht__t': 'Export Weight',
                  'WERTA__Ausfuhr:_Wert__Tsd._EUR': 'Export Value',
                  'GEWE__Einfuhr:_Gewicht__t': 'Import Weight',
                  'WERTE__Einfuhr:_Wert__Tsd._EUR': 'Import Value'
                 }

# Use the rename() method to replace column names
dt0_f = dt0_selected_columns.rename(columns=column_mapping)

filtered_df = dt0_f[(dt0_f['State'] == 'Berlin') & (dt0_f['Trade Group'] == 'Erzeugnisse der Landwirtschaft und Jagd') ]
dt0


# In[ ]:


unique_tradeGroups = dt0_f["Trade Group"].unique()
sorted_unique_tradeGroups = sorted(unique_tradeGroups)

# Print the sorted unique countries as a lis
# print(sorted_unique_tradeGroups)


# In[ ]:


####----- 51000-0034: (2008-2022) Exports and imports (foreign trade): Länder, years, classifications of trading goods -----####
selected_columns = ["Zeit", 
                    "1_Auspraegung_Code", 
                    "1_Auspraegung_Label", 
                    "2_Auspraegung_Code", 
                    "2_Auspraegung_Label", 
                    "GEWA__Ausfuhr:_Gewicht__t",
                    "WERTA__Ausfuhr:_Wert__Tsd._EUR",
                    "GEWE__Einfuhr:_Gewicht__t",
                    "WERTE__Einfuhr:_Wert__Tsd._EUR"]
dt1_selected_columns = dt1[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {'Zeit': 'Year',
                  '1_Auspraegung_Code': 'State Code',
                  '1_Auspraegung_Label': 'State',
                  '2_Auspraegung_Code': 'Commodity Code',
                  '2_Auspraegung_Label': 'Commodity Group',
                  'GEWA__Ausfuhr:_Gewicht__t': 'Export Weight',
                  'WERTA__Ausfuhr:_Wert__Tsd._EUR': 'Export Value',
                  'GEWE__Einfuhr:_Gewicht__t': 'Import Weight',
                  'WERTE__Einfuhr:_Wert__Tsd._EUR': 'Import Value'
                 }

# Use the rename() method to replace column names
dt1_f = dt1_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 51000-0036: 2018 Exports and imports (foreign trade): Länder, years, countries, classifications of trading goods -----####
selected_columns = ["Zeit", 
                    "1_Auspraegung_Code", 
                    "1_Auspraegung_Label", 
                    "2_Auspraegung_Code", 
                    "2_Auspraegung_Label", 
                    "3_Auspraegung_Code",
                    "3_Auspraegung_Label",
                    "GEWA__Ausfuhr:_Gewicht__t",
                    "WERTA__Ausfuhr:_Wert__Tsd._EUR",
                    "GEWE__Einfuhr:_Gewicht__t",
                    "WERTE__Einfuhr:_Wert__Tsd._EUR"]
dt2_selected_columns = dt2[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {'Zeit': 'Year',
                  '1_Auspraegung_Code':'Description Code',
                  '1_Auspraegung_Label': 'Country',
                  '2_Auspraegung_Code': 'State Code',
                  '2_Auspraegung_Label': 'State',
                  "3_Auspraegung_Code" : 'Trade Code' ,
                  "3_Auspraegung_Label" : 'Trade Group',
                  'GEWA__Ausfuhr:_Gewicht__t': 'Export Weight',
                  'WERTA__Ausfuhr:_Wert__Tsd._EUR': 'Export Value',
                  'GEWE__Einfuhr:_Gewicht__t': 'Import Weight',
                  'WERTE__Einfuhr:_Wert__Tsd._EUR': 'Import Value'
                 }

# Use the rename() method to replace column names
dt2_f = dt2_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 51000-0036: 2019 Exports and imports (foreign trade): Länder, years, countries, classifications of trading goods -----####
selected_columns = ["Zeit", "1_Auspraegung_Code", 
                    "1_Auspraegung_Label", 
                    "2_Auspraegung_Code", 
                    "2_Auspraegung_Label", 
                    "3_Auspraegung_Code",
                    "3_Auspraegung_Label",
                    "GEWA__Ausfuhr:_Gewicht__t",
                    "WERTA__Ausfuhr:_Wert__Tsd._EUR",
                    "GEWE__Einfuhr:_Gewicht__t",
                    "WERTE__Einfuhr:_Wert__Tsd._EUR"]
dt3_selected_columns = dt3[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {'Zeit': 'Year',
                  '1_Auspraegung_Code':'Description Code',
                  '1_Auspraegung_Label': 'Country',
                  '2_Auspraegung_Code': 'State Code',
                  '2_Auspraegung_Label': 'State',
                  "3_Auspraegung_Code" : 'Trade Code' ,
                  "3_Auspraegung_Label" : 'Trade Group',
                  'GEWA__Ausfuhr:_Gewicht__t': 'Export Weight',
                  'WERTA__Ausfuhr:_Wert__Tsd._EUR': 'Export Value',
                  'GEWE__Einfuhr:_Gewicht__t': 'Import Weight',
                  'WERTE__Einfuhr:_Wert__Tsd._EUR': 'Import Value'
                 }

# Use the rename() method to replace column names
dt3_f = dt3_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 51000-0036: 2020 Exports and imports (foreign trade): Länder, years, countries, classifications of trading goods -----####
selected_columns = ["Zeit", 
                    "1_Auspraegung_Code", 
                    "1_Auspraegung_Label", 
                    "2_Auspraegung_Code", 
                    "2_Auspraegung_Label", 
                    "3_Auspraegung_Code",
                    "3_Auspraegung_Label",
                    "GEWA__Ausfuhr:_Gewicht__t",
                    "WERTA__Ausfuhr:_Wert__Tsd._EUR",
                    "GEWE__Einfuhr:_Gewicht__t",
                    "WERTE__Einfuhr:_Wert__Tsd._EUR"]
dt4_selected_columns = dt4[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {'Zeit': 'Year',
                  '1_Auspraegung_Code':'Description Code',
                  '1_Auspraegung_Label': 'Country',
                  '2_Auspraegung_Code': 'State Code',
                  '2_Auspraegung_Label': 'State',
                  "3_Auspraegung_Code" : 'Trade Code' ,
                  "3_Auspraegung_Label" : 'Trade Group',
                  'GEWA__Ausfuhr:_Gewicht__t': 'Export Weight',
                  'WERTA__Ausfuhr:_Wert__Tsd._EUR': 'Export Value',
                  'GEWE__Einfuhr:_Gewicht__t': 'Import Weight',
                  'WERTE__Einfuhr:_Wert__Tsd._EUR': 'Import Value'
                 }

# Use the rename() method to replace column names
dt4_f = dt4_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 51000-0036: 2021 Exports and imports (foreign trade): Länder, years, countries, classifications of trading goods -----####
selected_columns = ["Zeit", 
                    "1_Auspraegung_Code", 
                    "1_Auspraegung_Label", 
                    "2_Auspraegung_Code", 
                    "2_Auspraegung_Label", 
                    "3_Auspraegung_Code",
                    "3_Auspraegung_Label",
                    "GEWA__Ausfuhr:_Gewicht__t",
                    "WERTA__Ausfuhr:_Wert__Tsd._EUR",
                    "GEWE__Einfuhr:_Gewicht__t",
                    "WERTE__Einfuhr:_Wert__Tsd._EUR"]
dt5_selected_columns = dt5[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {'Zeit': 'Year',
                  '1_Auspraegung_Code':'Description Code',
                  '1_Auspraegung_Label': 'Country',
                  '2_Auspraegung_Code': 'State Code',
                  '2_Auspraegung_Label': 'State',
                  "3_Auspraegung_Code" : 'Trade Code' ,
                  "3_Auspraegung_Label" : 'Trade Group',
                  'GEWA__Ausfuhr:_Gewicht__t': 'Export Weight',
                  'WERTA__Ausfuhr:_Wert__Tsd._EUR': 'Export Value',
                  'GEWE__Einfuhr:_Gewicht__t': 'Import Weight',
                  'WERTE__Einfuhr:_Wert__Tsd._EUR': 'Import Value'
                 }

# Use the rename() method to replace column names
dt5_f = dt5_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 51000-0036: 2022 Exports and imports (foreign trade): Länder, years, countries, classifications of trading goods -----####
selected_columns = ["Zeit", 
                    "1_Auspraegung_Code", 
                    "1_Auspraegung_Label", 
                    "2_Auspraegung_Code", 
                    "2_Auspraegung_Label", 
                    "3_Auspraegung_Code",
                    "3_Auspraegung_Label",
                    "GEWA__Ausfuhr:_Gewicht__t",
                    "WERTA__Ausfuhr:_Wert__Tsd._EUR",
                    "GEWE__Einfuhr:_Gewicht__t",
                    "WERTE__Einfuhr:_Wert__Tsd._EUR"]
dt6_selected_columns = dt6[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {'Zeit': 'Year',
                  '1_Auspraegung_Code':'Description Code',
                  '1_Auspraegung_Label': 'Country',
                  '2_Auspraegung_Code': 'State Code',
                  '2_Auspraegung_Label': 'State',
                  "3_Auspraegung_Code" : 'Trade Code' ,
                  "3_Auspraegung_Label" : 'Trade Group',
                  'GEWA__Ausfuhr:_Gewicht__t': 'Export Weight',
                  'WERTA__Ausfuhr:_Wert__Tsd._EUR': 'Export Value',
                  'GEWE__Einfuhr:_Gewicht__t': 'Import Weight',
                  'WERTE__Einfuhr:_Wert__Tsd._EUR': 'Import Value'
                 }

# Use the rename() method to replace column names
dt6_f = dt6_selected_columns.rename(columns=column_mapping)


# In[ ]:


#####----- 81000-0009: National accounts - Disposable income, saving of households: Germany, years -----####

# Get the column names
#column_names_dt7 = dt7.columns

selected_columns = ["time", 
                        "ETG004_____Compensation_of_employees_(national_concept)__Mrd._EUR",
       "SOZ002___-_Employers'_social_contributions__Mrd._EUR",
       "VST006___=_Gross_wages_and_salaries_(national_concept)__Mrd._EUR",
       "ABZ002___-_Income_tax_a._social_contributions_of_employees__Mrd._EUR",
       "VST013___=_Net_wages_and_salaries_(national_concept)__Mrd._EUR",
       "LST005___+_Social_benefits_other_than_soc._transf._in_kind__Mrd._EUR",
       "ABG003___-_Levies_on_social_benefits,_taxes_on_consumption__Mrd._EUR",
       "EKM003___=_Mass_income_(households)__Mrd._EUR",
       "VGR032___+_Op._surplus_/_mixed_income,_prop._income_of_hh__Mrd._EUR",
       "TRF005___+_Other_transfers_received_less_transfers_paid__Mrd._EUR",
       "EKM006___=_Disposable_income_of_households__Mrd._EUR",
       "EKM012___Mem._item:_Adjusted_disposable_income_of_househ.__Mrd._EUR",
       "VGR091______________Ch.in_net_eq._of_hh_in_pens.funds_res.__Mrd._EUR",
       "VGR037______________Saving_of_households__Mrd._EUR",
       "VGR092______________Reference_value_for_the_savings_ratio__Mrd._EUR",
       "VGR093______________Savings_ratio_of_households__percent"]
dt7_selected_columns = dt7[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {'time': 'Year',
       "ETG004_____Compensation_of_employees_(national_concept)__Mrd._EUR" : 'ETG004',
       "SOZ002___-_Employers'_social_contributions__Mrd._EUR" : 'SOZ002',
       "VST006___=_Gross_wages_and_salaries_(national_concept)__Mrd._EUR" : 'VST006',
       "ABZ002___-_Income_tax_a._social_contributions_of_employees__Mrd._EUR" : 'ABZ002',
       "VST013___=_Net_wages_and_salaries_(national_concept)__Mrd._EUR" : 'VST013',
       "LST005___+_Social_benefits_other_than_soc._transf._in_kind__Mrd._EUR" : 'LST005',
       "ABG003___-_Levies_on_social_benefits,_taxes_on_consumption__Mrd._EUR" : 'ABG003', 
       "EKM003___=_Mass_income_(households)__Mrd._EUR" : 'EKM003',
       "VGR032___+_Op._surplus_/_mixed_income,_prop._income_of_hh__Mrd._EUR" : 'VGR032',
       "TRF005___+_Other_transfers_received_less_transfers_paid__Mrd._EUR" : 'TRF005',
       "EKM006___=_Disposable_income_of_households__Mrd._EUR" : 'EKM006',
       "EKM012___Mem._item:_Adjusted_disposable_income_of_househ.__Mrd._EUR" : 'EKM012',
       "VGR091______________Ch.in_net_eq._of_hh_in_pens.funds_res.__Mrd._EUR" : 'VGR091',
       "VGR037______________Saving_of_households__Mrd._EUR" : 'VGR037',
       "VGR092______________Reference_value_for_the_savings_ratio__Mrd._EUR" : 'VGR092',
       "VGR093______________Savings_ratio_of_households__percent" :'VGR093'
                 }

# Use the rename() method to replace column names
dt7_f = dt7_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 81000-0015: National accounts - Employment, wages and salaries, hours worked: Germany, years, industries -----####
selected_columns = ["time", 
                    "2_variable_code.1", 
                    "2_variable_code.2", 
                    "ETG005__Compensation_of_employees_(domestic_concept)__Mrd._EUR", 
                    "VST020__Gross_wages_and_salaries_(domestic_concept)__Mrd._EUR", 
                    "ERW063__Persons_in_employment_(domestic_concept)__1000", 
                    "ERW061__Employees_(domestic_concept)__1000", 
                    "STD002__Hours_worked_by_persons_in_empl.(domestic_concept)__Mill._Std.", 
                    "STD003__Hours_worked_by_employees_(domestic_concept)__Mill._Std.", 
                    "STD007__Hours_worked_per_person_in_empl._(dom._concept)__Std.", 
                    "STD008__Hours_worked_per_employee_(domestic_concept)__Std."]

dt8_selected_columns = dt8[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {"time" : 'Year',
                    "2_variable_code.1" : 'Industry Code',
                    "2_variable_code.2" : 'Industry',
                    "ETG005__Compensation_of_employees_(domestic_concept)__Mrd._EUR" : 'ETG005',
                    "VST020__Gross_wages_and_salaries_(domestic_concept)__Mrd._EUR" : 'VST020',
                    "ERW063__Persons_in_employment_(domestic_concept)__1000" : 'ERW063', 
                    "ERW061__Employees_(domestic_concept)__1000" : 'ERW061',
                    "STD002__Hours_worked_by_persons_in_empl.(domestic_concept)__Mill._Std." : 'STD002', 
                    "STD003__Hours_worked_by_employees_(domestic_concept)__Mill._Std." : 'STD003',
                    "STD007__Hours_worked_per_person_in_empl._(dom._concept)__Std." : 'STD007',
                    "STD008__Hours_worked_per_employee_(domestic_concept)__Std." : 'STD008'
                 }

# Use the rename() method to replace column names
dt8_f = dt8_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 81000-0110_compensation_of_employees: National accounts - Compensation of employees: Germany, years, industries -----####
selected_columns = ["time", 
                    "2_variable_code.1", 
                    "2_variable_code.2", 
                    "ETG003__Compensation_of_employees__Mrd._EUR"]

dt9_selected_columns = dt9[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {"time" : 'Year',
                    "2_variable_code.1" : 'Industry Code',
                    "2_variable_code.2" : 'Industry',
                    "ETG003__Compensation_of_employees__Mrd._EUR" : 'ETG003'
                 }

# Use the rename() method to replace column names
dt9_f = dt9_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 81000-0110_compensation_of_employees_per_employee: National accounts - Compensation of employees: Germany, years, industries -----####
selected_columns = ["time", 
                    "2_variable_code.1", 
                    "2_variable_code.2", 
                    "ETG011__Compensation_of_employees_per_employee__EUR"]

dt10_selected_columns = dt10[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {"time" : 'Year',
                    "2_variable_code.1" : 'Industry Code',
                    "2_variable_code.2" : 'Industry',
                    "ETG011__Compensation_of_employees_per_employee__EUR" : 'ETG011'
                 }

# Use the rename() method to replace column names
dt10_f = dt10_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 81000-0110_compensation_of_employees_per_employee_hour: National accounts - Compensation of employees: Germany, years, industries -----####
selected_columns = ["time", 
                    "2_variable_code.1", 
                    "2_variable_code.2", 
                    "ETG012__Compensation_of_employees_per_employee_hour__EUR"]

dt11_selected_columns = dt11[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {"time" : 'Year',
                    "2_variable_code.1" : 'Industry Code',
                    "2_variable_code.2" : 'Industry',
                    "ETG012__Compensation_of_employees_per_employee_hour__EUR" : 'ETG012'
                 }

# Use the rename() method to replace column names
dt11_f = dt11_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 81000-0127: National accounts - Social contributions: Germany, years, types of social contributions -----####
selected_columns = ["time", 
                    "2_variable_code.1", 
                    "2_variable_code.2", 
                    "SOZ005__Social_contributions__Mrd._EUR"]

dt12_selected_columns = dt12[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {"time" : 'Year',
                    "2_variable_code.1" : 'Social Contribution Code',
                    "2_variable_code.2" : 'Type of Social Contribution',
                    "SOZ005__Social_contributions__Mrd._EUR" : 'SOZ005'
                 }

# Use the rename() method to replace column names
dt12_f = dt12_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 81000-0130_to_rest_of_the_world: National accounts - Revenue and expenditure from/to rest of the world: Germany, years, types of revenue and expenditure -----####
selected_columns = ["time", 
                    "2_variable_code.1", 
                    "2_variable_code.2", 
                    "ENM005__Revenue_and_expenditure_from/to_rest_of_the_world__Mrd._EUR"]

dt13_selected_columns = dt13[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {"time" : 'Year',
                    "2_variable_code.1" : 'Description Code',
                    "2_variable_code.2" : 'Type of Revenue and Expenditure',
                    "ENM005__Revenue_and_expenditure_from/to_rest_of_the_world__Mrd._EUR" : 'ENM005'
                 }

# Use the rename() method to replace column names
dt13_f = dt13_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 81000-0130_to_third_countries: National accounts - Revenue and expenditure from/to rest of the world: Germany, years, types of revenue and expenditure -----####
selected_columns = ["time", 
                    "2_variable_code.1", 
                    "2_variable_code.2", 
                    "ENM007__Revenue_and_expenditure_from/to_third_countries__Mrd._EUR"]

dt14_selected_columns = dt14[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {"time" : 'Year',
                    "2_variable_code.1" : 'Description Code',
                    "2_variable_code.2" : 'Type of Revenue and Expenditure',
                    "ENM007__Revenue_and_expenditure_from/to_third_countries__Mrd._EUR" : 'ENM007'
                 }

# Use the rename() method to replace column names
dt14_f = dt14_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 81000-0130_to_eu: National accounts - Revenue and expenditure from/to rest of the world: Germany, years, types of revenue and expenditure -----####
selected_columns = ["time", 
                    "2_variable_code.1", 
                    "2_variable_code.2", 
                    "ENM006__Revenue_and_expenditure_from/to_EU_Member_States__Mrd._EUR"]

dt15_selected_columns = dt15[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {"time" : 'Year',
                    "2_variable_code.1" : 'Description Code',
                    "2_variable_code.2" : 'Type of Revenue and Expenditure',
                    "ENM006__Revenue_and_expenditure_from/to_EU_Member_States__Mrd._EUR" : 'ENM006'
                 }

# Use the rename() method to replace column names
dt15_f = dt15_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 82111-0001: Regional accounts (production approach) - GDP at market prices (nominal): Länder, years -----####
selected_columns = ["Zeit", 
                    "1_Auspraegung_Code", 
                    "1_Auspraegung_Label", 
                    "BIP006__Bruttoinlandsprodukt_zu_Marktpreisen_i.jew.Preisen__Mill._EUR"]

dt16_selected_columns = dt16[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {"Zeit" : 'Year',
                    "1_Auspraegung_Code" : 'State Code',
                    "1_Auspraegung_Label" : 'State',
                    "BIP006__Bruttoinlandsprodukt_zu_Marktpreisen_i.jew.Preisen__Mill._EUR" : 'BIP006'
                 }

# Use the rename() method to replace column names
dt16_f = dt16_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 82111-0002: Regional accounts (production approach) - Gross value added at basic prices (nominal): Länder, years, economic sections -----####
selected_columns = ["time", 
                    "1_variable_code.1", 
                    "1_variable_code.2", 
                    "2_variable_code.1",
                    "2_variable_code.2",
                    "BWS008__Gross_value_added_at_basic_prices_(current_prices)__Mill._EUR"]

dt17_selected_columns = dt17[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {"time" : 'Year',
                    "1_variable_code.1" : 'State Code',
                    "1_variable_code.2" : 'State',
                    "2_variable_code.1" : 'Industry Code',
                    "2_variable_code.2" : 'Industry',
                    "BWS008__Gross_value_added_at_basic_prices_(current_prices)__Mill._EUR" : 'BWS008'
                 }

# Use the rename() method to replace column names
dt17_f = dt17_selected_columns.rename(columns=column_mapping)


# In[ ]:


####----- 82411-0001: Regional accounts (redistribution accounts) - Disposable income of households: Länder, years -----####
selected_columns = ["Zeit", 
                    "1_Auspraegung_Code", 
                    "1_Auspraegung_Label", 
                    "EKM006__Verfuegb.Einkommen_d.priv.Haushalte_(Ausgabenkzpt)__Mill._EUR",
                    "EKM014__Verfuegb._Einkommen_d._priv._Haushalte_je_Einwohner__EUR"]

dt18_selected_columns = dt18[selected_columns]

# Create a dictionary to map old column names to new column names
column_mapping = {"Zeit" : 'Year',
                    "1_Auspraegung_Code" : 'State Code',
                    "1_Auspraegung_Label" : 'State',
                    "EKM006__Verfuegb.Einkommen_d.priv.Haushalte_(Ausgabenkzpt)__Mill._EUR" : 'EKM006',
                    "EKM014__Verfuegb._Einkommen_d._priv._Haushalte_je_Einwohner__EUR" : 'EKM014'
                 }

# Use the rename() method to replace column names
dt18_f = dt18_selected_columns.rename(columns=column_mapping)


# In[ ]:


###############################################################################################################################
# -------------------------------------VISUALIZATION------------------------------------- #
###############################################################################################################################
# -------------------------------------TEXT ------------------------------------- #
###############################################################################################################################

project_text = PreText(text="""
Institute: InfAI
Project: CoyPu

Data Visualization Dashboard Demo 
""",width=650, height=100)

#inputs = column(project_text, data_table) #bokeh_app = pn.pane.Bokeh(inputs) #bokeh_project_text = pn.pane.Bokeh(project_text)


# In[ ]:


###############################################################################################################################
# -------------------------------------SANKEY DIAGRAM FOR TRADE GROUPS PER STATE (TAB1) ------------------------------------- #
###############################################################################################################################

import plotly.graph_objects as go

# PREPARE DATA FOR SANKEY DIAGRAM
# Filter rows for the specific year
filtered_df = dt0_f[dt0_f['Year'] == 2022]
filtered_df = dt0_f[(dt0_f['State'] == "Baden-Württemberg") & (dt0_f['Year'] == 2022)]

# Create a new DataFrame with the other three columns
new_df = filtered_df[['State', 'Trade Group', 'Export Weight','Export Value','Import Weight','Import Value']]

new_df.loc[:, 'Export Weight'] = new_df['Export Weight'].str.replace(',', '.').astype(float)
new_df.loc[:, 'Export Value'] = new_df['Export Value'].str.replace(',', '.').astype(float)
new_df.loc[:, 'Import Weight'] = new_df['Import Weight'].str.replace(',', '.').astype(float)
new_df.loc[:, 'Import Value'] = new_df['Import Value'].str.replace(',', '.').astype(float)

# Get unique 'State' and 'Trade Group' values
states = sorted(new_df['State'].unique())
trade_groups = sorted(new_df['Trade Group'].unique())

# Create a list of node labels by concatenating unique 'State' and 'Trade Group' values
node_labels = list(states) + list(trade_groups)

# Create a mapping from labels to node indices
label_to_index = {label: index for index, label in enumerate(node_labels)}

# Create a single source index (assuming it's the first source in your data)
source = label_to_index[states[0]]

# Create target indices based on 'Trade Group' values
targets = [label_to_index[trade_group] for trade_group in new_df['Trade Group']]

# Generate a list of unique colors for each connection link
unique_targets = new_df['Trade Group'].unique()
#colors = [f'rgb({i % 256}, {i % 128}, {i % 64})' for i in range(len(unique_targets))]
colors =  '#A6E3D7'

# Create a Sankey diagram using Plotly
fig = go.Figure(go.Sankey(
    node=dict(
        pad=15,
        thickness=20,
        line=dict(color="black", width=0.5),
        label=node_labels,
        #x=[0, 1],  # 0 for source node, 1 for target nodes
        #y=[0.5] * (len(node_labels)),  # All nodes are centered vertically
    ),
    link=dict(
        source=[source] * len(targets),  # Single source index repeated for each target
        target=targets,
        value=new_df['Export Weight'],
        color=colors,  # Assign different colors to each connection link 
        #text=hover_text  # Assign the hover text list
        #hovertemplate='Source: %{source.customdata}<br>Target: %{source.customdata}<br>Weight: %{value}<br>Text:', 
        #test=hover_text
        #hoverinfo = f'%{hover_text}',
        
    )
    
))

# Customize the layout
fig.update_layout(
    title='Product Groups in total imports/exports. Please select state/year/output type to visualize.',
    font_size=9,
)

# Define a callback function to update the filtered_df and the Sankey diagram
def update_sankey(event):
    selected_state = select_state.value
    selected_year = select_year.value  # Get the selected year
    selected_export_import = select_export_import.value
    
    # Filter rows for the specific year
    # filtered_df = dt0_f[dt0_f['Year'] == 2022]
    filtered_df = dt0_f[(dt0_f['State'] == selected_state) & (dt0_f['Year'] == selected_year)]


    # Create a new DataFrame with the other three columns
    new_df = filtered_df[['State', 'Trade Group', 'Export Weight','Export Value','Import Weight','Import Value']]

    new_df.loc[:, selected_export_import] = new_df[selected_export_import].str.replace(',', '.').astype(float)
    #print(new_df)

    #print(new_df['State'].values.tolist())
    #print(sorted(new_df['State'].unique()))

    # Get unique 'State' and 'Trade Group' values
    states = sorted(new_df['State'].unique())
    trade_groups = sorted(new_df['Trade Group'].unique())

    # Create a list of node labels by concatenating unique 'State' and 'Trade Group' values
    node_labels = list(states) + list(trade_groups)

    # Create a mapping from labels to node indices
    label_to_index = {label: index for index, label in enumerate(node_labels)}

    # Create a single source index (assuming it's the first source in your data)
    source = label_to_index[states[0]]

    # Create target indices based on 'Trade Group' values
    targets = [label_to_index[trade_group] for trade_group in new_df['Trade Group']]

    # Generate a list of unique colors for each connection link
    unique_targets = new_df['Trade Group'].unique()
    #colors = [f'rgb({i % 256}, {i % 128}, {i % 64})' for i in range(len(unique_targets))]
    colors =  '#A6E3D7'
     
    # Update the Sankey diagram
    new_df = filtered_df[['State', 'Trade Group', selected_export_import]]
    new_df[selected_export_import] = new_df[selected_export_import].str.replace(',', '.').astype(float)
    
    # Create target indices based on 'Trade Group' values
    targets = [label_to_index[trade_group] for trade_group in new_df['Trade Group']]
    
    # Update the Sankey diagram data
    fig.data[0].link.target = targets
    fig.data[0].link.value = new_df[selected_export_import]
    fig.data[0].node.label = node_labels
    
    # Update the plot title
    #fig.update_layout(title=f'Trade groups for {selected_state} in {selected_year}')
    
     # Update the plot title
    if selected_export_import == 'Export Weight':
        fig.update_layout(title=f'<b> Net Mass (Tons) of Trade Groups for {selected_state} in {selected_year}<br>')
    elif selected_export_import == 'Import Weight':
         fig.update_layout(title=f'<b> Net Mass (Tons) of Trade Groups for {selected_state} in {selected_year}<br>')
    else:
        fig.update_layout(title=f'<b> Value (Tsd. EUR) of Trade Groups for {selected_state} in {selected_year}<br>')


# Define the select_state widget  
list_of_states = sorted(dt0_f['State'].unique())
select_state = pn.widgets.Select(name='Select State:', options=list_of_states, width=200, value="Sachsen")

list_of_years = sorted(dt0_f['Year'].unique())
select_year = pn.widgets.Select(name='Select Year:', options=list_of_years,width=100)

list_of_export_import = ['Export Weight','Export Value','Import Weight','Import Value']
select_export_import = pn.widgets.Select(name='Select Output:', options=list_of_export_import, width=150, value="Import Value")


# Add the callback function to the select_state widget
select_state.param.watch(update_sankey, 'value')
select_year.param.watch(update_sankey, 'value')
select_export_import.param.watch(update_sankey, 'value')

#pn.extension('plotly')
panel_layout_sankey_chart =pn.pane.Plotly(fig)
# Set the height of the HoloViews plot (Sankey)
panel_layout_sankey_chart.height = 1000 
panel_layout_sankey_chart.width = 850
# panel_layout_sankey_chart.width = 600

# if we use panel_layout by pn.pane, when we serve the app, it will automatically get the panel layout configuration parameters.


# In[ ]:


###############################################################################################################################
# -------------------------------------TAB1 LINE PLOTS ------------------------------------- #
###############################################################################################################################

from bokeh.models.widgets import DateRangeSlider
from bokeh.models import NumeralTickFormatter
from bokeh.themes import built_in_themes
from bokeh.models import FixedTicker


filtered_df = dt0_f[(dt0_f['State'] == "Baden-Württemberg") & (dt0_f['Trade Group'] == "Chemische Erzeugnisse") & (dt0_f['Year'] >= 2008) & (dt0_f['Year'] <= 2022)]

filtered_df.loc[:, 'Export Weight'] = filtered_df['Export Weight'].str.replace(',', '.').astype(float)
filtered_df.loc[:, 'Import Weight'] = filtered_df['Import Weight'].str.replace(',', '.').astype(float)
filtered_df.loc[:, 'Export Value'] = filtered_df['Export Value'].str.replace(',', '.').astype(float)
filtered_df.loc[:, 'Import Value'] = filtered_df['Import Value'].str.replace(',', '.').astype(float)


def transform_data(trade_group, state, year_range):
    start_year, end_year = year_range    
    filtered_df = dt0_f[(dt0_f['State'] == state) & 
                            (dt0_f['Trade Group'] == trade_group) & 
                            (dt0_f['Year'] >= start_year) & (dt0_f['Year'] <= end_year)]
    filtered_df.loc[:, 'Export Weight'] = filtered_df['Export Weight'].str.replace(',', '.').astype(float)
    filtered_df.loc[:, 'Import Weight'] = filtered_df['Import Weight'].str.replace(',', '.').astype(float)
    filtered_df.loc[:, 'Export Value'] = filtered_df['Export Value'].str.replace(',', '.').astype(float)
    filtered_df.loc[:, 'Import Value'] = filtered_df['Import Value'].str.replace(',', '.').astype(float)
    #data = filtered_df[['Year', 'Export Weight']]
    return filtered_df

def create_plot(trade_group="Erze", state="Sachsen", year_range=[2012,2018]):
    data = transform_data(trade_group, state, year_range) 
    
    x = list(data['Year'])
    y = list(data['Export Weight'])

    TOOLTIPS = [
        ('year', "@x"),
        ('export weight (tons)', "@y{0,0.0}"),
    ]

    p = figure(width=700, height=230, x_axis_label="years", y_axis_label="net mass (tons)", title=f'{trade_group} in {state} between {year_range[0]} and {year_range[1]} years',tools='hover,wheel_zoom,save', tooltips=TOOLTIPS)
    # add a line renderer with legend and line thickness
    p.line(x, y,  line_width=1, line_color="navy",legend_label="Export: Net mass") #legend_label="Net Mass in Tons"
    # Format the y-axis labels with commas
    p.yaxis.formatter = NumeralTickFormatter(format="0,0.0")
    p.title.text_font_size = '9pt'
    p.xaxis.axis_label_text_font_size = "9pt"
    p.yaxis.axis_label_text_font_size = "9pt"
    p.xaxis.axis_label_text_font_style = "italic"   
    p.legend.label_text_font = "times"
    p.legend.label_text_font_style = "italic"
    p.legend.label_text_color = "navy"
    p.legend.location = "top_left"
    p.xaxis.ticker = FixedTicker(ticks=x)
    # ref: https://docs.bokeh.org/en/2.4.2/docs/user_guide/styling.html
    return p

def create_plot_exp_v(trade_group="Erze", state="Sachsen", year_range=[2012,2018]):
    data = transform_data(trade_group, state, year_range) 
    
    x = list(data['Year'])
    y = list(data['Export Value'])

    TOOLTIPS = [
        ('year', "@x"),
        ('export value in Tsd. EUR', "@y{0,0.0}"),
    ]

    p = figure(width=700, height=230, x_axis_label="years", y_axis_label="value in Tsd. EUR", title=f'{trade_group} in {state} between {year_range[0]} and {year_range[1]} years',tools='hover,wheel_zoom,save', tooltips=TOOLTIPS)
    # add a line renderer with legend and line thickness
    p.line(x, y,  line_width=1, line_color="navy",legend_label="Export: value in Tsd. EUR") #legend_label="Net Mass in Tons"
    # Format the y-axis labels with commas
    p.yaxis.formatter = NumeralTickFormatter(format="0,0.0")
    p.title.text_font_size = '9pt'
    p.xaxis.axis_label_text_font_size = "9pt"
    p.yaxis.axis_label_text_font_size = "9pt"
    p.xaxis.axis_label_text_font_style = "italic"   
    p.legend.label_text_font = "times"
    p.legend.label_text_font_style = "italic"
    p.legend.label_text_color = "navy"
    p.legend.location = "top_left"
    p.xaxis.ticker = FixedTicker(ticks=x)
    # ref: https://docs.bokeh.org/en/2.4.2/docs/user_guide/styling.html
    return p

def create_plot_imp_w(trade_group="Erze", state="Sachsen", year_range=[2012,2018]):
    data = transform_data(trade_group, state, year_range) 
    
    x = list(data['Year'])
    y = list(data['Import Weight'])

    TOOLTIPS = [
        ('year', "@x"),
        ('import weight (tons)', "@y{0,0.0}"),
    ]

    p = figure(width=700, height=230, x_axis_label="years", y_axis_label="net mass (tons)", title=f'{trade_group} in {state} between {year_range[0]} and {year_range[1]} years',tools='hover,wheel_zoom,save', tooltips=TOOLTIPS)
    # add a line renderer with legend and line thickness
    p.line(x, y,  line_width=1, line_color="brown",legend_label="Import: Net mass") #legend_label="Net Mass in Tons"
    # Format the y-axis labels with commas
    p.yaxis.formatter = NumeralTickFormatter(format="0,0.0")
    p.title.text_font_size = '9pt'
    p.xaxis.axis_label_text_font_size = "9pt"
    p.yaxis.axis_label_text_font_size = "9pt"
    p.xaxis.axis_label_text_font_style = "italic"   
    p.legend.label_text_font = "times"
    p.legend.label_text_font_style = "italic"
    p.legend.label_text_color = "brown"
    p.legend.location = "top_left"
    p.xaxis.ticker = FixedTicker(ticks=x)
    # ref: https://docs.bokeh.org/en/2.4.2/docs/user_guide/styling.html
    return p

def create_plot_imp_v(trade_group="Erze", state="Sachsen", year_range=[2012,2018]):
    data = transform_data(trade_group, state, year_range) 
    
    x = list(data['Year'])
    y = list(data['Import Value'])

    TOOLTIPS = [
        ('year', "@x"),
        ('import value in Tsd. EUR', "@y{0,0.0}"), #"@y{0.0}"
    ]

    p = figure(width=700, height=230, x_axis_label="years", y_axis_label="value in Tsd. EUR", title=f'{trade_group} in {state} between {year_range[0]} and {year_range[1]} years',tools='hover,wheel_zoom,save', tooltips=TOOLTIPS)
    # add a line renderer with legend and line thickness
    p.line(x, y,  line_width=1, line_color="brown",legend_label="Import: value in Tsd. EUR") #legend_label="Net Mass in Tons"
    # Format the y-axis labels with commas
    p.yaxis.formatter = NumeralTickFormatter(format="0,0.0")
    p.title.text_font_size = '9pt'
    p.xaxis.axis_label_text_font_size = "9pt"
    p.yaxis.axis_label_text_font_size = "9pt"
    p.xaxis.axis_label_text_font_style = "italic"   
    p.legend.label_text_font = "times"
    p.legend.label_text_font_style = "italic"
    p.legend.label_text_color = "brown"
    p.legend.location = "top_left"
    p.xaxis.ticker = FixedTicker(ticks=x)
    # ref: https://docs.bokeh.org/en/2.4.2/docs/user_guide/styling.html
    return p

create_plot(trade_group="Erze", state="Sachsen", year_range=[2012,2018])
create_plot_exp_v(trade_group="Erze", state="Sachsen", year_range=[2012,2018])
create_plot_imp_w(trade_group="Erze", state="Sachsen", year_range=[2012,2018])
create_plot_imp_v(trade_group="Erze", state="Sachsen", year_range=[2012,2018])

list_of_trade_groups = sorted(dt0_f['Trade Group'].unique())
list_of_states_plot = sorted(dt0_f['State'].unique())

select_trade_group_plot = pn.widgets.Select(name='Select Product Group:', options=list_of_trade_groups, width=150)
select_state_plot = pn.widgets.Select(name='Select State:', options=list_of_states_plot, width=200, value="Sachsen")
year_range_slider = pn.widgets.RangeSlider(name='Year Range Slider', start=sorted(dt0_f['Year'].unique())[0], end=sorted(dt0_f['Year'].unique())[-1], value=(2015, 2020), step=1)

bound_plot = pn.bind(create_plot, trade_group=select_trade_group_plot, state=select_state_plot, year_range=year_range_slider)
bound_plot_exp_v = pn.bind(create_plot_exp_v, trade_group=select_trade_group_plot, state=select_state_plot, year_range=year_range_slider)
bound_plot_imp_w = pn.bind(create_plot_imp_w, trade_group=select_trade_group_plot, state=select_state_plot, year_range=year_range_slider)
bound_plot_imp_v = pn.bind(create_plot_imp_v, trade_group=select_trade_group_plot, state=select_state_plot, year_range=year_range_slider)


# In[ ]:


###############################################################################################################################
# ---------------------------- SANKEY DIAGRAM FOR EXPORT/IMPORT COUNTRIES PER STATE (TAB2 LAYOUT) --------------------------- #
###############################################################################################################################

combined_df_sankey = pd.concat([dt2_f, dt3_f, dt4_f, dt5_f , dt6_f], ignore_index=True)

# Filter rows for the specific year
filtered_df_tg = combined_df_sankey[(combined_df_sankey['Trade Group'] == "Bekleidung") & (combined_df_sankey['Year'] == 2022) & (combined_df_sankey['State'] == "Sachsen")]

# Create a new DataFrame with the other three columns
new_df_tg = filtered_df_tg[['Year','Country', 'State', 'Trade Group', 'Export Weight','Export Value','Import Weight','Import Value']]

new_df_tg.loc[:, 'Export Weight'] = new_df_tg['Export Weight'].str.replace(',', '.').astype(float)
new_df_tg.loc[:, 'Export Value'] = new_df_tg['Export Value'].str.replace(',', '.').astype(float)
new_df_tg.loc[:, 'Import Weight'] = new_df_tg['Import Weight'].str.replace(',', '.').astype(float)
new_df_tg.loc[:, 'Import Value'] = new_df_tg['Import Value'].str.replace(',', '.').astype(float)


# Get unique 'State' and 'Trade Group' values    
trade_groups = sorted(new_df_tg['Trade Group'].unique())
countries = sorted(new_df_tg['Country'].unique())

# Create a list of node labels by concatenating unique 'State' and 'Trade Group' values
node_labels = list(trade_groups) + list(countries)

# Create a mapping from labels to node indices
label_to_index = {label: index for index, label in enumerate(node_labels)}

# Create a single source index (assuming it's the first source in your data)
source = label_to_index[trade_groups[0]]

# Create target indices based on 'Trade Group' values
targets = [label_to_index[trade_group] for trade_group in new_df_tg['Country']]

# Generate a list of unique colors for each connection link
unique_targets = new_df_tg['Country'].unique()
#colors = [f'rgb({i % 256}, {i % 128}, {i % 64})' for i in range(len(unique_targets))]
colors =  '#D2E6FF'

#hover_text = new_df_tg['Export Value'].tolist()
#hover_text = [str(value) for value in hover_text]


# Create a Sankey diagram using Plotly
fig2 = go.Figure(go.Sankey(
    node=dict(
        pad=15,
        thickness=20,
        line=dict(color="black", width=0.5),
        label=node_labels,
        #x=[0, 1],  # 0 for source node, 1 for target nodes
        #y=[0.5] * (len(node_labels)),  # All nodes are centered vertically
    ),
    link=dict(
        source=[source] * len(targets),  # Single source index repeated for each target
        target=targets,
        value=new_df_tg['Export Weight'],
        color=colors,  # Assign different colors to each connection link        
    )
))

# Customize the layout
fig2.update_layout(
    title='Trade groups for export and import countries.',
    font_size=9,
)

# Define a callback function to update the filtered_df and the Sankey diagram
def update_sankey_trade(event):
    selected_state_tg = select_state_tg.value
    selected_year_tg = select_year_tg.value  # Get the selected year
    selected_trade_group = select_trade_group.value
    selected_export_import_tg = select_export_import_tg.value
       
    # Filter rows for the specific year
    filtered_df = combined_df_sankey[(combined_df_sankey['Trade Group'] == selected_trade_group) & (combined_df_sankey['Year'] == selected_year_tg) & (combined_df_sankey['State'] == selected_state_tg)]
    #filtered_df = dt0_f[(dt0_f['State'] == selected_state) & (dt0_f['Year'] == selected_year)]
    #print(filtered_df)

    # Create a new DataFrame with the other three columns
    new_df_tg = filtered_df[['Year','Country', 'State', 'Trade Group', 'Export Weight','Export Value','Import Weight','Import Value']]
    new_df_tg.loc[:, selected_export_import_tg] = new_df_tg[selected_export_import_tg].str.replace(',', '.').astype(float)

    # Get unique 'Trade Group' and 'Country' values    
    trade_groups = sorted(new_df_tg['Trade Group'].unique())
    countries = sorted(new_df_tg['Country'].unique())
    
    # Create a list of node labels by concatenating unique 'State' and 'Trade Group' values
    node_labels = list(trade_groups) + list(countries)

    # Create a mapping from labels to node indices
    label_to_index = {label: index for index, label in enumerate(node_labels)}

    # Create a single source index (assuming it's the first source in your data)
    source = label_to_index[trade_groups[0]]

    # Create target indices based on 'Trade Group' values
    targets = [label_to_index[country] for country in new_df_tg['Country']]

    # Generate a list of unique colors for each connection link
    unique_targets = new_df_tg['Country'].unique()
    #colors = [f'rgb({i % 256}, {i % 128}, {i % 64})' for i in range(len(unique_targets))]
    colors =  '#00FF00' #'#A6E3D7'
     
    # Update the Sankey diagram
    new_df_tg = new_df_tg[['Trade Group', 'Country', selected_export_import_tg]]
    new_df_tg[selected_export_import_tg] = new_df_tg[selected_export_import_tg].astype(float)
    
    # Create target indices based on 'Trade Group' values
    targets = [label_to_index[country] for country in new_df_tg['Country']]
    
    # Update the Sankey diagram data
    fig2.data[0].link.target = targets
    fig2.data[0].link.value = new_df_tg[selected_export_import_tg]
    fig2.data[0].node.label = node_labels
    
    # Update the plot title
    if selected_export_import_tg == 'Export Weight':
        fig2.update_layout(title=f'<b>"{selected_trade_group}" Proportion by Country<br> (in Tons) for {selected_state_tg} in {selected_year_tg}.')
    elif selected_export_import_tg == 'Import Weight':
        fig2.update_layout(title=f'<b>"{selected_trade_group}" Proportion by Country<br> (in Tons) for {selected_state_tg} in {selected_year_tg}.')
    else:
        fig2.update_layout(title=f'<b>"{selected_trade_group}" Proportion by Country<br> (Tsd. EUR) for {selected_state_tg} in {selected_year_tg}.')

# Define the select_state widget
list_of_states_tg = sorted(combined_df_sankey['State'].unique())
select_state_tg = pn.widgets.Select(name='Select State:', options=list_of_states_tg, width=150, value="Sachsen")

list_of_years_tg = sorted(combined_df_sankey['Year'].unique())
select_year_tg = pn.widgets.Select(name='Select Year:', options=list_of_years_tg,width=100)

list_of_export_import_tg = ['Export Weight','Export Value','Import Weight','Import Value']
select_export_import_tg = pn.widgets.Select(name='Select Output:', options=list_of_export_import_tg, width=125, value="Import Value")

list_of_trade_groups = sorted(combined_df_sankey['Trade Group'].unique())
select_trade_group = pn.widgets.Select(name='Select Product Group:', options=list_of_trade_groups, width=150)

# Add the callback function to the select_state widget
select_state_tg.param.watch(update_sankey_trade, 'value')
select_year_tg.param.watch(update_sankey_trade, 'value')
select_export_import_tg.param.watch(update_sankey_trade, 'value')
select_trade_group.param.watch(update_sankey_trade, 'value')

panel_layout_sankey_chart_2 =pn.pane.Plotly(fig2)
# Set the height of the HoloViews plot (Sankey)
panel_layout_sankey_chart_2.height = 1700 
panel_layout_sankey_chart_2.width = 850
# panel_layout_sankey_chart_2.width = 600

# Filter the DataFrame for Belgium and Kohle trade group
filtered_df_test = combined_df_sankey[(combined_df_sankey['Trade Group'] == "Bekleidung") & (combined_df_sankey['Country'] == "Türkei") & (combined_df_sankey['State'] == "Sachsen")]
# Convert 'Import Weight' to numeric (if not already)
filtered_df_test.loc[:, "Import Weight"] = filtered_df_test["Import Weight"].str.replace(',', '.').astype(float)
import_weight_sum = filtered_df_test['Import Weight'].sum()

unique_countries = combined_df_sankey['Country'].unique()


# In[ ]:


###############################################################################################################################
# ---------------------------- TAB2 LINE PLOTS --------------------------- #
###############################################################################################################################

combined_df_sankey = pd.concat([dt2_f, dt3_f, dt4_f, dt5_f , dt6_f], ignore_index=True)

# Create a new DataFrame with the other three columns
new_df_tg = combined_df_sankey[['Year','Country', 'State', 'Trade Group', 'Export Weight','Export Value','Import Weight','Import Value']]

new_df_tg.loc[:, 'Export Weight'] = new_df_tg['Export Weight'].str.replace(',', '.').astype(float)
new_df_tg.loc[:, 'Export Value'] = new_df_tg['Export Value'].str.replace(',', '.').astype(float)
new_df_tg.loc[:, 'Import Weight'] = new_df_tg['Import Weight'].str.replace(',', '.').astype(float)
new_df_tg.loc[:, 'Import Value'] = new_df_tg['Import Value'].str.replace(',', '.').astype(float)

def transform_data_country(trade_group, state, country, year_range):
    start_year, end_year = year_range    
    filtered_df_tg = combined_df_sankey[(new_df_tg['Trade Group'] == trade_group) & 
                                        (new_df_tg['State'] == state) &
                                        (new_df_tg['Country'] == country) &
                                        (new_df_tg['Year'] >= start_year) & 
                                        (new_df_tg['Year'] <= end_year)]
    filtered_df_tg.loc[:, 'Export Weight'] = filtered_df_tg['Export Weight'].str.replace(',', '.').astype(float)
    filtered_df_tg.loc[:, 'Import Weight'] = filtered_df_tg['Import Weight'].str.replace(',', '.').astype(float)
    filtered_df_tg.loc[:, 'Export Value'] = filtered_df_tg['Export Value'].str.replace(',', '.').astype(float)
    filtered_df_tg.loc[:, 'Import Value'] = filtered_df_tg['Import Value'].str.replace(',', '.').astype(float)
    return filtered_df_tg

def transform_data_all(trade_group, state, year_range):
    start_year, end_year = year_range    
    filtered_df = dt0_f[(dt0_f['State'] == state) & 
                            (dt0_f['Trade Group'] == trade_group) & 
                            (dt0_f['Year'] >= start_year) & (dt0_f['Year'] <= end_year)]
    filtered_df.loc[:, 'Export Weight'] = filtered_df['Export Weight'].str.replace(',', '.').astype(float)
    filtered_df.loc[:, 'Import Weight'] = filtered_df['Import Weight'].str.replace(',', '.').astype(float)
    filtered_df.loc[:, 'Export Value'] = filtered_df['Export Value'].str.replace(',', '.').astype(float)
    filtered_df.loc[:, 'Import Value'] = filtered_df['Import Value'].str.replace(',', '.').astype(float)
    return filtered_df

def create_plot_country(trade_group="Bekleidung", state="Sachsen", country="Türkei", year_range=[2019,2022]):
    
    if country == "All countries":
        data = transform_data_all(trade_group, state, year_range) 
    else:
        data = transform_data_country(trade_group, state, country, year_range) 
    
    x = list(data['Year'])
    y = list(data['Export Weight'])
    
    TOOLTIPS = [
        ('year', "@x"),
        ('export weight (tons)', "@y{0,0.0}"),
    ]
    
    plot_country = figure(width=720, height=230, x_axis_label="years", y_axis_label="net mass (tons)", title=f'{trade_group} in {state} between {year_range[0]} and {year_range[1]} years',tools='hover, reset, wheel_zoom,save', tooltips=TOOLTIPS)
    # add a line renderer with legend and line thickness
    plot_country.line(x, y,  line_width=1, line_color="navy",legend_label="Export: Net mass") #legend_label="Net Mass in Tons"
    # Format the y-axis labels with commas
    plot_country.yaxis.formatter = NumeralTickFormatter(format="0,0.0")
    plot_country.title.text_font_size = '9pt'
    plot_country.xaxis.axis_label_text_font_size = "9pt"
    plot_country.yaxis.axis_label_text_font_size = "9pt"
    plot_country.xaxis.axis_label_text_font_style = "italic"   
    plot_country.legend.label_text_font = "times"
    plot_country.legend.label_text_font_style = "italic"
    plot_country.legend.label_text_color = "navy"
    plot_country.legend.location = "top_left"
    plot_country.xaxis.ticker = FixedTicker(ticks=x)
    # ref: https://docs.bokeh.org/en/2.4.2/docs/user_guide/styling.html
    return plot_country

def create_plot_country_exp_v(trade_group="Bekleidung", state="Sachsen", country="Türkei", year_range=[2019,2022]):
    if country == "All countries":
        data = transform_data_all(trade_group, state, year_range) 
    else:
        data = transform_data_country(trade_group, state, country, year_range) 
    
    x = list(data['Year'])
    y = list(data['Export Value'])
    
    TOOLTIPS = [
        ('year', "@x"),
        ('export value in Tsd. EUR', "@y{0,0.0}"),
    ]
    
    plot_country = figure(width=720, height=230, x_axis_label="years", y_axis_label="value in Tsd. EUR", title=f'{trade_group} in {state} between {year_range[0]} and {year_range[1]} years',tools='hover, reset, wheel_zoom,save', tooltips=TOOLTIPS)
    # add a line renderer with legend and line thickness
    plot_country.line(x, y,  line_width=1, line_color="navy",legend_label="Export: value in Tsd. EUR") #legend_label="Net Mass in Tons"
    # Format the y-axis labels with commas
    plot_country.yaxis.formatter = NumeralTickFormatter(format="0,0.0")
    plot_country.title.text_font_size = '9pt'
    plot_country.xaxis.axis_label_text_font_size = "9pt"
    plot_country.yaxis.axis_label_text_font_size = "9pt"
    plot_country.xaxis.axis_label_text_font_style = "italic"   
    plot_country.legend.label_text_font = "times"
    plot_country.legend.label_text_font_style = "italic"
    plot_country.legend.label_text_color = "navy"
    plot_country.legend.location = "top_left"
    plot_country.xaxis.ticker = FixedTicker(ticks=x)
    # ref: https://docs.bokeh.org/en/2.4.2/docs/user_guide/styling.html
    return plot_country


def create_plot_country_imp_w(trade_group="Bekleidung", state="Sachsen", country="All countries", year_range=[2019,2022]):
    if country == "All countries":
        data = transform_data_all(trade_group, state, year_range) 
    else:
        data = transform_data_country(trade_group, state, country, year_range) 
    
    x = list(data['Year'])
    y = list(data['Import Weight'])
    
    TOOLTIPS = [
        ('year', "@x"),
        ('import weight (tons)', "@y{0,0.0}"),
    ]
    
    plot_country = figure(width=720, height=230, x_axis_label="years", y_axis_label="net mass (tons)", title=f'{trade_group} in {state} between {year_range[0]} and {year_range[1]} years',tools='hover, reset, wheel_zoom,save', tooltips=TOOLTIPS)
    # add a line renderer with legend and line thickness
    plot_country.line(x, y,  line_width=1, line_color="brown",legend_label="Import: Net mass") #legend_label="Net Mass in Tons"
    # Format the y-axis labels with commas
    plot_country.yaxis.formatter = NumeralTickFormatter(format="0,0.0")
    plot_country.title.text_font_size = '9pt'
    plot_country.xaxis.axis_label_text_font_size = "9pt"
    plot_country.yaxis.axis_label_text_font_size = "9pt"
    plot_country.xaxis.axis_label_text_font_style = "italic"   
    plot_country.legend.label_text_font = "times"
    plot_country.legend.label_text_font_style = "italic"
    plot_country.legend.label_text_color = "brown"
    plot_country.legend.location = "top_left"
    plot_country.xaxis.ticker = FixedTicker(ticks=x)
    # ref: https://docs.bokeh.org/en/2.4.2/docs/user_guide/styling.html
    return plot_country


def create_plot_country_imp_v(trade_group="Bekleidung", state="Sachsen", country="Türkei", year_range=[2019,2022]):
    if country == "All countries":
        data = transform_data_all(trade_group, state, year_range) 
    else:
        data = transform_data_country(trade_group, state, country, year_range) 
    
    x = list(data['Year'])
    y = list(data['Import Value'])
    
    TOOLTIPS = [
        ('year', "@x"),
        ('import value in Tsd. EUR', "@y{0,0.0}"),
    ]
    
    plot_country = figure(width=720, height=230, x_axis_label="years", y_axis_label="value in Tsd. EUR", title=f'{trade_group} in {state} between {year_range[0]} and {year_range[1]} years',tools='hover, reset, wheel_zoom,save', tooltips=TOOLTIPS)
    # add a line renderer with legend and line thickness
    plot_country.line(x, y,  line_width=1, line_color="brown",legend_label="Import: value in Tsd. EUR") #legend_label="Net Mass in Tons"
    # Format the y-axis labels with commas
    plot_country.yaxis.formatter = NumeralTickFormatter(format="0,0.0")
    plot_country.title.text_font_size = '9pt'
    plot_country.xaxis.axis_label_text_font_size = "9pt"
    plot_country.yaxis.axis_label_text_font_size = "9pt"
    plot_country.xaxis.axis_label_text_font_style = "italic"   
    plot_country.legend.label_text_font = "times"
    plot_country.legend.label_text_font_style = "italic"
    plot_country.legend.label_text_color = "brown"
    plot_country.legend.location = "top_left"
    plot_country.xaxis.ticker = FixedTicker(ticks=x)
    # ref: https://docs.bokeh.org/en/2.4.2/docs/user_guide/styling.html
    return plot_country

create_plot_country(trade_group="Bekleidung", state="Sachsen", country= "Türkei", year_range=[2019,2022])
create_plot_country_exp_v(trade_group="Bekleidung", state="Sachsen", country= "Türkei", year_range=[2019,2022])
create_plot_country_imp_w(trade_group="Bekleidung", state="Sachsen", country= "Türkei", year_range=[2019,2022])
create_plot_country_imp_v(trade_group="Bekleidung", state="Sachsen", country= "Türkei", year_range=[2019,2022])

list_of_trade_groups_c = sorted(new_df_tg['Trade Group'].unique())
list_of_states_c = sorted(new_df_tg['State'].unique())
list_of_countries = sorted(new_df_tg['Country'].unique())
list_of_countries.insert(0, "All countries")

select_trade_groups_c = pn.widgets.Select(name='Select Product Group:', options=list_of_trade_groups_c, width=150)
select_states_c = pn.widgets.Select(name='Select State:', options=list_of_states_c, width=150, value="Sachsen")
select_country= pn.widgets.Select(name='Select Country:', options=list_of_countries, width=150, value="Türkei")
year_range_slider_c = pn.widgets.RangeSlider(name='Year Range Slider', start=sorted(new_df_tg['Year'].unique())[0], end=sorted(new_df_tg['Year'].unique())[-1], value=(2019, 2022), step=1, width=200)

bound_plot_c = pn.bind(create_plot_country, trade_group=select_trade_groups_c, state=select_states_c, country= select_country, year_range=year_range_slider_c)
bound_plot_c_exp_v = pn.bind(create_plot_country_exp_v, trade_group=select_trade_groups_c, state=select_states_c, country= select_country, year_range=year_range_slider_c)
bound_plot_c_imp_w = pn.bind(create_plot_country_imp_w, trade_group=select_trade_groups_c, state=select_states_c, country= select_country, year_range=year_range_slider_c)
bound_plot_c_imp_v = pn.bind(create_plot_country_imp_v, trade_group=select_trade_groups_c, state=select_states_c, country= select_country, year_range=year_range_slider_c)


# In[ ]:


###############################################################################################################################
# ---------------------------- TAB3 BAR PLOTS --------------------------- #
###############################################################################################################################

from bokeh.models import ColumnDataSource
from bokeh.palettes import *
from bokeh.plotting import figure, show
from bokeh.transform import factor_cmap
from math import pi
new_df = dt0_f[['Year', 'State', 'Trade Group', 'Export Weight','Export Value','Import Weight','Import Value']]

new_df.loc[:, 'Export Weight'] = new_df['Export Weight'].str.replace(',', '.').astype(float)
new_df.loc[:, 'Import Weight'] = new_df['Import Weight'].str.replace(',', '.').astype(float)
new_df.loc[:, 'Export Value'] = new_df['Export Value'].str.replace(',', '.').astype(float)
new_df.loc[:, 'Import Value'] = new_df['Import Value'].str.replace(',', '.').astype(float)


def transform_data_states_tg(trade_group, bar_year):
    year = bar_year    
    filtered_new_df = new_df[(new_df['Trade Group'] == trade_group) & (new_df['Year'] == year)]
    return filtered_new_df

def create_bar_plot_tg(trade_group="Bekleidung", select_output = 'Export Weight', bar_year=2022):
    data = transform_data_states_tg(trade_group, bar_year) 
    
    states = list (data['State'])
    values = list (data[select_output])

    if select_output == 'Export Weight':
        TOOLTIPS = [('year', "@states"),('export volume in tons', "@values{0,0.0}"),]
        title = f'"{trade_group}" export volume distribution in {bar_year} for the states of Germany.'
    elif select_output == 'Import Weight':
        TOOLTIPS = [('year', "@states"),('import volume in tons', "@values{0,0.0}"),]
        title = f'"{trade_group}" import volume distribution in {bar_year} for the states of Germany.'
    elif select_output == 'Export Value':
        TOOLTIPS = [('year', "@states"),('export value in Tsd. EUR', "@values{0,0.0}"),]
        title = f'"{trade_group}" export value distribution in {bar_year} for the states of Germany.'
    else:
        TOOLTIPS = [('year', "@states"),('import value in Tsd. EUR', "@values{0,0.0}"),]
        title = f'"{trade_group}" import value distribution in {bar_year} for the states of Germany.'

    source = ColumnDataSource(data=dict(states=states, values=values))

    # Create a color mapper based on the 'State' column
    color_mapper = factor_cmap(field_name='states', palette=TolRainbow[len(states)], factors=states)

    bar_plot_tg = figure(x_range=states, height=350,width=720, title=title, tools='hover,reset,wheel_zoom', tooltips=TOOLTIPS)  # toolbar_location=None : it removes the toolbar on the right corner

    bar_plot_tg.vbar(x='states', top='values', width=0.6, source=source, line_color='white', fill_color=color_mapper) #legend_field="states",
    bar_plot_tg.yaxis.formatter = NumeralTickFormatter(format="0,0.0")
    bar_plot_tg.xgrid.grid_line_color = None
    
    if select_output == 'Export Weight':
        bar_plot_tg.yaxis.axis_label = "net mass in tons"
    elif select_output == 'Import Weight':
        bar_plot_tg.yaxis.axis_label = "net mass in tons"
    elif select_output == 'Export Value':
        bar_plot_tg.yaxis.axis_label = "value in Tsd. EUR"
    else:
        bar_plot_tg.yaxis.axis_label = "value in Tsd. EUR"
        
    bar_plot_tg.title.text_font_size = '8pt'
    bar_plot_tg.xaxis.axis_label_text_font_size = "9pt"
    bar_plot_tg.yaxis.axis_label_text_font_size = "9pt"
    bar_plot_tg.xaxis.major_label_orientation = pi/4
    return bar_plot_tg



create_bar_plot_tg(trade_group="Bekleidung", select_output='Export Weight', bar_year=2022)
create_bar_plot_tg(trade_group="Bekleidung", select_output='Export Value', bar_year=2022)
create_bar_plot_tg(trade_group="Bekleidung", select_output='Import Weight', bar_year=2022)
create_bar_plot_tg(trade_group="Bekleidung", select_output='Import Value', bar_year=2022)

list_of_trade_groups_bar = sorted(new_df['Trade Group'].unique())
select_trade_groups_bar = pn.widgets.Select(name='Select Trade Group:', options=list_of_trade_groups_bar, width=250)

list_of_years_bar = sorted(new_df['Year'].unique())
select_year_bar = pn.widgets.Select(name='Select Year:', options=list_of_years_bar,width=100)

bound_bar_plot_tg = pn.bind(create_bar_plot_tg, trade_group=select_trade_groups_bar, select_output='Export Weight', bar_year=select_year_bar)
bound_bar_plot_tg_exp_v = pn.bind(create_bar_plot_tg, trade_group=select_trade_groups_bar, select_output='Export Value', bar_year=select_year_bar)
bound_bar_plot_tg_imp_w = pn.bind(create_bar_plot_tg, trade_group=select_trade_groups_bar, select_output='Import Weight', bar_year=select_year_bar)
bound_bar_plot_tg_imp_v = pn.bind(create_bar_plot_tg, trade_group=select_trade_groups_bar, select_output='Import Value', bar_year=select_year_bar)


# In[ ]:


###############################################################################################################################
# TREEMAP for trade groups' proportional comparison in states as well as total in Germany for selected years #
###############################################################################################################################

import plotly.express as px

def transform_data_treemap(state_name, year_range):
    if state_name == 'Total in Germany':
        start_year, end_year = year_range    

        filtered_df = dt0_f[(dt0_f['Year'] >= start_year) &
                        (dt0_f['Year'] <= end_year)]

        # Convert the 'Export Weight' column to a numeric data type
        filtered_df.loc[:,'Export Weight'] = filtered_df.loc[:,'Export Weight'].str.replace(',', '.').astype(float)
        filtered_df.loc[:,'Import Weight'] = filtered_df.loc[:,'Import Weight'].str.replace(',', '.').astype(float)
        filtered_df.loc[:,'Export Value'] = filtered_df.loc[:,'Export Value'].str.replace(',', '.').astype(float)
        filtered_df.loc[:,'Import Value'] = filtered_df.loc[:,'Import Value'].str.replace(',', '.').astype(float)
        return filtered_df
        
    else:       
        start_year, end_year = year_range    

        filtered_df = dt0_f[(dt0_f['State'] == state_name) &
                        (dt0_f['Year'] >= start_year) &
                        (dt0_f['Year'] <= end_year)]

        # Convert the 'Export Weight' column to a numeric data type
        filtered_df.loc[:,'Export Weight'] = filtered_df.loc[:,'Export Weight'].str.replace(',', '.').astype(float)
        filtered_df.loc[:,'Import Weight'] = filtered_df.loc[:,'Import Weight'].str.replace(',', '.').astype(float)
        filtered_df.loc[:,'Export Value'] = filtered_df.loc[:,'Export Value'].str.replace(',', '.').astype(float)
        filtered_df.loc[:,'Import Value'] = filtered_df.loc[:,'Import Value'].str.replace(',', '.').astype(float)
        return filtered_df


def create_treemap(state_name, year_range, result):
    output = result
    data = transform_data_treemap(state_name, year_range)   
    trade_groups = sorted(data['Trade Group'].unique())    
    start_year, end_year = year_range
    state_sums_exp_w = {}
    state_sums_exp_v = {}
    state_sums_imp_w = {}
    state_sums_imp_v = {}
    
    for trade_group in trade_groups:
        # Calculate the sum of 'Export Weight' for the state and trade group
        sum_export_exp_weight = data['Export Weight'].sum()
        sum_export_exp_value = data['Export Value'].sum()
        sum_export_imp_weight = data['Import Weight'].sum()
        sum_export_imp_value  = data['Import Value'].sum()

        state_sums_exp_w[trade_group] = sum_export_exp_weight
        state_sums_exp_v[trade_group] = sum_export_exp_value
        state_sums_imp_w[trade_group] = sum_export_imp_weight
        state_sums_imp_v[trade_group] = sum_export_imp_value
    
    # Group the DataFrame by 'Trade Group' and calculate the sum of 'Export Weight' for each group
    grouped_data = data.groupby('Trade Group')[output].sum()

    sum_of_tg = 0
    for i in grouped_data.values:
        sum_of_tg += i
    #print (sum_of_tg)
    sum_of_tg = f"{sum_of_tg:,.1f}"
    
    if output == "Export Weight" or output == "Import Weight":
        sum_of_tg = str(sum_of_tg) + " tons"
    else:
        sum_of_tg = str(sum_of_tg) + " Tsd. EUR"
        
    tradeGroupList = []
    weight_sum = []
    
    for key, value in grouped_data.items():
        tradeGroupList.append(key)
        weight_sum.append(int(value))
    
    new_df_tree = pd.DataFrame({'Trade Group': tradeGroupList, output: weight_sum})
    new_df_tree = new_df_tree[new_df_tree[output] != 0]

    # Calculate the total sum of the "Weight" column
    total_weight = new_df_tree[output].sum()

    # Calculate the percentage of each row
    new_df_tree['Percentage'] = (new_df_tree[output] / total_weight) * 100

    # Combine "Trade Group" and "Percentage" columns to create custom labels
    new_df_tree['Custom_Label'] = ('<span style="font-size: 14px;  font-family: Arial, sans-serif;"><b>' + 
                               new_df_tree['Trade Group'] + '</b></span>' + ' <br>' + 
                               '<span style="font-size: 18px; "><br><b>' + 
                               new_df_tree['Percentage'].round(2).astype(str) + 
                               '%</b></span>')
    # font-weight: bold; font-style: italic; font-family: Arial, sans-serif;
    # Create a new DataFrame for plotting with custom labels
    plot_df = pd.DataFrame({
        'Trade Groups': ['Trade Groups'] * len(new_df_tree),
        'Trade Group': new_df_tree['Custom_Label'],
         output: new_df_tree[output],
        'Percentage': new_df_tree['Percentage']})

    # Specify the color scale (e.g., 'Viridis', 'Blues', 'Reds', 'YlOrRd', etc.)
    color_scale = 'Brwnyl' #Viridis

    # Calculate the midpoint for the color scale
    color_midpoint = np.average(new_df_tree[output], weights=new_df_tree['Percentage'])

    # Plot the treemap
    plotly_treemap = px.treemap(plot_df, path=['Trade Groups', 'Trade Group'], values=output,
                                color=output, labels={'Weight': output},
                                color_continuous_scale=color_scale, color_continuous_midpoint=color_midpoint)
                                # hover_data=['Percentage']
    # Print available information in the traces
    #for trace in plotly_treemap.data:
        #print("Available keys in this trace:")
        #print(trace)
        #print("Custom data:")
        #print(trace.hovertemplate)

    if output == 'Export Weight':
        plotly_treemap.update_coloraxes(colorbar_title_text='tons') # <br><br> Percentage=%{customdata[0]}
        plotly_treemap.update_traces(hovertemplate="<b>ID=%{id}<br><br> Parent=%{parent}<br><br> Total Export Weight (tons) =%{value}<br> <extra></extra>")
    elif output == 'Import Weight':
        plotly_treemap.update_coloraxes(colorbar_title_text='tons')
        plotly_treemap.update_traces(hovertemplate="<b>ID=%{id}<br><br> Parent=%{parent}<br><br> Total Import Weight (tons) =%{value}<br> <extra></extra>")
    elif output == 'Export Value':
        plotly_treemap.update_coloraxes(colorbar_title_text='Tsd. EUR')
        plotly_treemap.update_traces(hovertemplate="<b>ID=%{id}<br><br> Parent=%{parent}<br><br> Total Export Value (Tsd. EUR) =%{value}<br> <extra></extra>")
    else:
        plotly_treemap.update_coloraxes(colorbar_title_text='Tsd. EUR')
        plotly_treemap.update_traces(hovertemplate="<b>ID=%{id}<br><br> Parent=%{parent}<br><br> Total Import Value (Tsd. EUR) =%{value}<br> <extra></extra>")

    # Update the color axis to set a custom label for the midpoint
    #plotly_treemap.update_coloraxes(colorbar_title_text='in tons')
    plotly_treemap.update_layout(paper_bgcolor="#E0F7FA")
    
    # Modify the layout to increase font size
    plotly_treemap.update_layout(uniformtext_minsize=12) # , plot_bgcolor='black'
    if year_range[0] == year_range[1]:
        plotly_treemap.update_layout(
        title=f'Proportional comparison of trade groups in {state_name} in {start_year}.<br>{output} in total: {sum_of_tg}',  # Set the title text
        title_font=dict(
            family='Times New Roman',       # Font family (e.g., 'Arial', 'Times New Roman', 'sans-serif')
            size=17,              # Font size (in points)
            color='black',),         # Font color
        )
    else:
        plotly_treemap.update_layout(
        #title=f'Proportional comparison of trade groups in {state_name} between {start_year} and {end_year}.<br>{output} in total: {sum_of_tg}',  # Set the title text
        title=f'{output} in total: {sum_of_tg}',  # Set the title text
        title_font=dict(
            family='Times New Roman',       # Font family (e.g., 'Arial', 'Times New Roman', 'sans-serif')
            size=17,              # Font size (in points)
            color='black',),         # Font color
        )
    # Adjust the size of the figure
    plotly_treemap.update_layout(width=1500, height=650)  # Change the values as needed

    return plotly_treemap


  
create_treemap(state_name="Sachsen", year_range=[2018,2022], result="Export Weight")
       
list_of_states_tree = sorted(dt0_f['State'].unique())

select_states_tree = pn.widgets.Select(name='Show per State or Total', groups={'State': list_of_states_tree, 'Other option': ['Total in Germany']}, value="Sachsen")
#select_states_tree = pn.widgets.Select(name='Show per State or Total', options=list_of_states_tree, width=250)
year_range_s = pn.widgets.RangeSlider(name='Year Range:', start=sorted(dt0_f['Year'].unique())[0], end=sorted(dt0_f['Year'].unique())[-1], value=(2018, 2022), step=1, width=200)
select_treemap_output = pn.widgets.Select(name='Select Input Data', options=['Export Weight','Export Value','Import Weight','Import Value'], width=250, value="Import Value")


bound_treemap_plot = pn.bind(create_treemap, state_name=select_states_tree, year_range=year_range_s, result=select_treemap_output)



# In[ ]:


###############################################################################################################################
# TREEMAP for Commodity groups' proportional comparison in states as well as total in Germany for selected years #
###############################################################################################################################


def transform_data_treemap_pg(state_name, year_range):
    if state_name == 'Total in Germany':
        start_year, end_year = year_range    

        filtered_df = dt1_f[(dt1_f['Year'] >= start_year) &
                        (dt1_f['Year'] <= end_year)]

        # Convert the 'Export Weight' column to a numeric data type
        filtered_df.loc[:,'Export Weight'] = filtered_df.loc[:,'Export Weight'].str.replace(',', '.').astype(float)
        filtered_df.loc[:,'Import Weight'] = filtered_df.loc[:,'Import Weight'].str.replace(',', '.').astype(float)
        filtered_df.loc[:,'Export Value'] = filtered_df.loc[:,'Export Value'].str.replace(',', '.').astype(float)
        filtered_df.loc[:,'Import Value'] = filtered_df.loc[:,'Import Value'].str.replace(',', '.').astype(float)
        return filtered_df
        
    else:       
        start_year, end_year = year_range    

        filtered_df = dt1_f[(dt1_f['State'] == state_name) &
                        (dt1_f['Year'] >= start_year) &
                        (dt1_f['Year'] <= end_year)]

        # Convert the 'Export Weight' column to a numeric data type
        filtered_df.loc[:,'Export Weight'] = filtered_df.loc[:,'Export Weight'].str.replace(',', '.').astype(float)
        filtered_df.loc[:,'Import Weight'] = filtered_df.loc[:,'Import Weight'].str.replace(',', '.').astype(float)
        filtered_df.loc[:,'Export Value'] = filtered_df.loc[:,'Export Value'].str.replace(',', '.').astype(float)
        filtered_df.loc[:,'Import Value'] = filtered_df.loc[:,'Import Value'].str.replace(',', '.').astype(float)
        return filtered_df


def create_treemap_pg(state_name, year_range, result):
    output = result
    data = transform_data_treemap_pg(state_name, year_range)
    #print(data)
   
    trade_groups = sorted(data['Commodity Group'].unique())
    
    start_year, end_year = year_range
    state_sums_exp_w = {}
    state_sums_exp_v = {}
    state_sums_imp_w = {}
    state_sums_imp_v = {}
    
    for trade_group in trade_groups:
        # Calculate the sum of 'Export Weight' for the state and trade group
        sum_export_exp_weight = data['Export Weight'].sum()
        sum_export_exp_value = data['Export Value'].sum()
        sum_export_imp_weight = data['Import Weight'].sum()
        sum_export_imp_value  = data['Import Value'].sum()

        state_sums_exp_w[trade_group] = sum_export_exp_weight
        state_sums_exp_v[trade_group] = sum_export_exp_value
        state_sums_imp_w[trade_group] = sum_export_imp_weight
        state_sums_imp_v[trade_group] = sum_export_imp_value
        # print(sum_export_exp_weight)
    
    data = data.drop(data[data['Commodity Group'] == 'Waren insgesamt'].index)
    
    # Group the DataFrame by 'Trade Group' and calculate the sum of 'Export Weight' for each group
    grouped_data = data.groupby('Commodity Group')[output].sum()
      
    sum_of_cg = 0
    for i in grouped_data.values:
        sum_of_cg += i
    #print (sum_of_tg)
    sum_of_cg = f"{sum_of_cg:,.1f}"
    
    if output == "Export Weight" or output == "Import Weight":
        sum_of_cg = str(sum_of_cg) + " tons"
    else:
        sum_of_cg = str(sum_of_cg) + " Tsd. EUR"
    
    tradeGroupList = []
    weight_sum = []
    
    for key, value in grouped_data.items():
        tradeGroupList.append(key)
        weight_sum.append(int(value))

    #print(tradeGroupList)
    #print(weight_sum)
    
    new_df_tree = pd.DataFrame({'Commodity Group': tradeGroupList, output: weight_sum})
    new_df_tree = new_df_tree[new_df_tree[output] != 0]

    # Calculate the total sum of the "Weight" column
    total_weight = new_df_tree[output].sum()

    # Calculate the percentage of each row
    new_df_tree['Percentage'] = (new_df_tree[output] / total_weight) * 100

    # Combine "Trade Group" and "Percentage" columns to create custom labels
    #new_df_tree['Custom_Label'] = new_df_tree['Commodity Group'] + '<br>(' + new_df_tree['Percentage'].round(2).astype(str) + '%)'
    #new_df_tree['Custom_Label'] = '<span style="font-size: 14px;  font-family: Arial, sans-serif;">' + new_df_tree['Commodity Group'] + '</span>' + ' <br>(' + '<span style="font-size: 17px; ">' + + new_df_tree['Percentage'].round(2).astype(str) + '%</span>)'
    new_df_tree['Custom_Label'] = ('<span style="font-size: 14px;  font-family: Arial, sans-serif;"><b>' + 
                               new_df_tree['Commodity Group'] + '</b></span>' + '<br>' + 
                               '<span style="font-size: 18px; "><br><b>' + 
                               new_df_tree['Percentage'].round(2).astype(str) + 
                               '%</b></span>')
    # Create a new DataFrame for plotting with custom labels
    plot_df = pd.DataFrame({
        'Commodity Groups': ['Commodity Groups'] * len(new_df_tree),
        'Commodity Group': new_df_tree['Custom_Label'],
        output: new_df_tree[output],
        'Percentage': new_df_tree['Percentage']})

    # Specify the color scale (e.g., 'Viridis', 'Blues', 'Reds', 'YlOrRd', etc.)
    color_scale = 'blugrn' #aggrnyl

    # Calculate the midpoint for the color scale
    color_midpoint = np.average(new_df_tree[output], weights=new_df_tree['Percentage'])

    # Plot the treemap
    plotly_treemap_pg = px.treemap(plot_df, path=['Commodity Groups', 'Commodity Group'], values=output,
                                color=output, labels={'Weight': output},
                                color_continuous_scale=color_scale, color_continuous_midpoint=color_midpoint)
                                # hover_data=['Percentage']
    
    if output == 'Export Weight':
        # Update the color axis to set a custom label for the midpoint
        plotly_treemap_pg.update_coloraxes(colorbar_title_text='tons')
        # Update the hover data with our customized input text
        plotly_treemap_pg.update_traces(hovertemplate="<b>ID=%{id}<br><br> Parent=%{parent}<br><br> Total Export Weight (tons) =%{value}<br> <extra></extra>")
    elif output == 'Import Weight':
        plotly_treemap_pg.update_coloraxes(colorbar_title_text='tons')
        plotly_treemap_pg.update_traces(hovertemplate="<b>ID=%{id}<br><br> Parent=%{parent}<br><br> Total Import Weight (tons) =%{value}<br> <extra></extra>")
    elif output == 'Export Value':
        plotly_treemap_pg.update_coloraxes(colorbar_title_text='Tsd. EUR')
        plotly_treemap_pg.update_traces(hovertemplate="<b>ID=%{id}<br><br> Parent=%{parent}<br><br> Total Export Value (Tsd. EUR) =%{value}<br> <extra></extra>")
    else:
        plotly_treemap_pg.update_coloraxes(colorbar_title_text='Tsd. EUR')
        plotly_treemap_pg.update_traces(hovertemplate="<b>ID=%{id}<br><br> Parent=%{parent}<br><br> Total Import Value (Tsd. EUR) =%{value}<br> <extra></extra>")    
    
    plotly_treemap_pg.update_layout(paper_bgcolor="#E0F7FA")
    
    # Modify the layout to increase font size
    plotly_treemap_pg.update_layout(uniformtext_minsize=12) # , plot_bgcolor='black'
    if year_range[0] == year_range[1]:
        plotly_treemap_pg.update_layout(
        title=f'Proportional comparison of commodity groups in {state_name} in {start_year}.<br>{output} in total: {sum_of_cg}',  # Set the title text
        title_font=dict(
            family='Times New Roman',       # Font family (e.g., 'Arial', 'Times New Roman', 'sans-serif')
            size=17,              # Font size (in points)
            color='black',),         # Font color
        )
    else:
        plotly_treemap_pg.update_layout(
        #title=f'Proportional comparison of commodity groups in {state_name} between {start_year} and {end_year}.<br>{output} in total: {sum_of_cg}',  # Set the title text
        title=f'{output} in total: {sum_of_cg}',  # Set the title text
        title_font=dict(
            family='Times New Roman',       # Font family (e.g., 'Arial', 'Times New Roman', 'sans-serif')
            size=17,              # Font size (in points)
            color='black',),         # Font color
        )
    # Adjust the size of the figure
    plotly_treemap_pg.update_layout(width=1500, height=650)  # Change the values as needed

    return plotly_treemap_pg

create_treemap_pg(state_name="Sachsen", year_range=[2018,2022], result="Import Value")
       
#list_of_states_tree_pg = sorted(dt1_f['State'].unique())
#select_states_tree_pg = pn.widgets.Select(name='Show per State or Total', groups={'State': list_of_states_tree_pg, 'Other option': ['Total in Germany']})
#year_range_s_pg = pn.widgets.RangeSlider(name='Year Range:', start=sorted(dt0_f['Year'].unique())[0], end=sorted(dt0_f['Year'].unique())[-1], value=(2018, 2022), step=1, width=200)

bound_treemap_plot_pg = pn.bind(create_treemap_pg, state_name=select_states_tree, year_range=year_range_s, result=select_treemap_output)


# In[ ]:


###############################################################################################################################
# Choropleth Map Visualisation #
# It can be implemented in the future, it works but currently not used, just example #
###############################################################################################################################


from bokeh.plotting import figure
from bokeh.models import GeoJSONDataSource, ColorBar, HoverTool, LinearColorMapper
from bokeh.transform import linear_cmap
from bokeh.palettes import Viridis256
import geopandas as gpd
import random
import numpy as np  # Import numpy for random number generation

def create_choropleth_map(state_name):
    # Load GeoJSON data for Saxony districts using GeoPandas
    
    if state_name == "Baden-Württemberg":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataBadenW.geojson'))
    elif state_name == "Bayern":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataBaveria.geojson'))
    elif state_name == "Berlin":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataBerlin.json'))
    elif state_name == "Brandenburg":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataBrandenburg.geojson'))
    elif state_name == "Bremen":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataBremen.json'))
    elif state_name == "Hamburg":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataHamburg.json'))
    elif state_name == "Hessen":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataHesse.geojson'))
    elif state_name == "Mecklenburg-Vorpommern":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataMecklenburg.json'))
    elif state_name == "Niedersachsen":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataLowerSaxony.json'))
    elif state_name == "Nordrhein-Westfalen":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataNRW.json'))
    elif state_name == "Rheinland-Pfalz":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataRheinelandPalatinate.geojson'))
    elif state_name == "Saarland":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataSaarland.json'))
    elif state_name == "Sachsen":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataSaxony.json'))
    elif state_name == "Sachsen-Anhalt":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataSaxonyAnhalt.json'))
    elif state_name == "Schleswig-Holstein":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataSchleswigHolstein.geojson'))
    elif state_name == "Thüringen":        
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataThuringia.geojson'))
    elif state_name == "Germany":
        districts_gdf = gpd.read_file(os.path.join(p_dir, 'GeoDataGermany', 'geodataGermanyStates.geojson'))
    else:
        print("State name is not in the state_name list.")
        None
    
    # Generate random values for each district and store them in a numpy array
    np.random.seed(37)  # Set a seed for reproducibility
    random_values = np.random.randint(1, 100, size=len(districts_gdf))
    districts_gdf["value"] = random_values.tolist()  # Convert numpy array to a list

    # Create a GeoJSONDataSource for districts
    geo_source = GeoJSONDataSource(geojson=districts_gdf.to_json())

    # Define a fixed color palette (you can customize this palette as needed)
    palette = Viridis256

    # Define a LinearColorMapper
    color_mapper = LinearColorMapper(palette=palette, low=min(random_values), high=max(random_values))

    # Create the figure
    choropleth_map = figure(title="Saxony Districts Map", width=800, height=600)

    if state_name == "Baden-Württemberg":
        choropleth_map = figure(title= f'{state_name}: District Map', width=2000, height=2000)
    elif state_name == "Bayern":
                choropleth_map = figure(title= f'{state_name} State: District Map', width=2600, height=2600)
    elif state_name == "Berlin":
        choropleth_map = figure(title= f'{state_name}: District Map', width=1000, height=800)
    elif state_name == "Brandenburg":
        choropleth_map = figure(title= f'{state_name}: District Map', width=1000, height=800)
    elif state_name == "Bremen":
        choropleth_map = figure(title= f'{state_name}: District Map', width=1000, height=1200)
    elif state_name == "Hamburg":
        choropleth_map = figure(title= f'{state_name}: District Map', width=800, height=600)
    elif state_name == "Hessen":
        choropleth_map = figure(title= f'{state_name}: District Map', width=1200, height=1600)
    elif state_name == "Mecklenburg-Vorpommern":
        choropleth_map = figure(title= f'{state_name}: District Map', width=2000, height=1600)
    elif state_name == "Niedersachsen":
        choropleth_map = figure(title= f'{state_name}: District Map', width=1500, height=1600)
    elif state_name == "Nordrhein-Westfalen":
        choropleth_map = figure(title= f'{state_name}: District Map', width=1200, height=1400)
    elif state_name == "Rheinland-Pfalz":
        choropleth_map = figure(title= f'{state_name}: District Map', width=1000, height=1250)
    elif state_name == "Saarland":
        choropleth_map = figure(title= f'{state_name}: District Map', width=1200, height=1000)
    elif state_name == "Sachsen":
        choropleth_map = figure(title= f'{state_name}: District Map', width=1100, height=850)
    elif state_name == "Sachsen-Anhalt":
        choropleth_map = figure(title= f'{state_name}: District Map', width=900, height=1200)
    elif state_name == "Schleswig-Holstein":
        choropleth_map = figure(title= f'{state_name}: District Map', width=950, height=1200)
    elif state_name == "Thüringen":        
        choropleth_map = figure(title= f'{state_name}: District Map', width=1200, height=1000)
    elif state_name == "Germany":
        choropleth_map = figure(title= f'{state_name}: District Map', width=1900, height=2500)
    else:
        None    
        
    
    # Add the GeoJSON patch glyphs for districts with the colormap
    patches = choropleth_map.patches("xs", "ys", source=geo_source, fill_color={'field': 'value', 'transform': color_mapper}, line_color="black", line_width=0.5)

    # Add district names and centroids to the figure
    for _, district in districts_gdf.iterrows():
        centroid = district.geometry.centroid
        choropleth_map.text(x=centroid.x, y=centroid.y, text=[district["GEN"]], text_align="center", text_baseline="middle", text_font_size="10pt", text_color="black")

    # Add a color bar
    color_bar = ColorBar(color_mapper=color_mapper, width=8, location=(0, 0))
    choropleth_map.add_layout(color_bar, 'right')

    # Add hover tool
    hover = HoverTool(
        tooltips=[
            ("District", "@GEN"),
            ("Value", "@value"),
        ],
        renderers=[patches],
    )
    choropleth_map.add_tools(hover)
    
    return choropleth_map


create_choropleth_map(state_name="Sachsen")

list_of_states_choropleth = sorted(['Baden-Württemberg', 'Bayern', 'Berlin', 'Brandenburg', 'Bremen', 'Hamburg', 
                              'Hessen', 'Mecklenburg-Vorpommern','Niedersachsen', 'Nordrhein-Westfalen',
                              'Rheinland-Pfalz', 'Saarland', 'Sachsen', 'Sachsen-Anhalt', 'Schleswig-Holstein', 'Thüringen'])

select_states_choropleth = pn.widgets.Select(name='Select Germany or specific state map:',groups={'District Level State Map:': list_of_states_choropleth,'Federals States of:': ['Germany']}, value="Sachsen")

bound_choropleth_plot = pn.bind(create_choropleth_map, state_name=select_states_choropleth)


# In[ ]:


from bokeh.models import FixedTicker

new_cg_df = dt1_f[['Year', 'State', 'Commodity Group', 'Export Weight','Export Value','Import Weight','Import Value']]

new_cg_df.loc[:, 'Export Weight'] = new_cg_df['Export Weight'].str.replace(',', '.').astype(float)
new_cg_df.loc[:, 'Import Weight'] = new_cg_df['Import Weight'].str.replace(',', '.').astype(float)
new_cg_df.loc[:, 'Export Value'] = new_cg_df['Export Value'].str.replace(',', '.').astype(float)
new_cg_df.loc[:, 'Import Value'] = new_cg_df['Import Value'].str.replace(',', '.').astype(float)



def transform_data_cg_plot(commodity_group, state, cg_year):
    start_year, end_year = cg_year    
    filtered_cg_df = new_cg_df[(new_cg_df['State'] == state) & 
                               (new_cg_df['Commodity Group'] == commodity_group) & 
                               (new_cg_df['Year'] >= start_year) & (new_cg_df['Year'] <= end_year)]
    return filtered_cg_df

def create_cg_plot(commodity_group="Rohstoffe", state="Sachsen", select_output = 'Export Weight', cg_year=[2018,2022]):
    
    data = transform_data_cg_plot(commodity_group, state, cg_year)  
   
    x = list(data['Year'])
    y = list(data[select_output])

    if select_output == 'Export Weight':
        TOOLTIPS = [('year', "@x"),('export volume (tons)', "@y{0,0.0}"),]
        title = f'"{commodity_group}" export volume distribution in {cg_year} for the states of Germany.'
    elif select_output == 'Import Weight':
        TOOLTIPS = [('year', "@x"),('import volume (tons)', "@y{0,0.0}"),]
        title = f'"{commodity_group}" import volume distribution in {cg_year} for the states of Germany.'
    elif select_output == 'Export Value':
        TOOLTIPS = [('year', "@x"),('export value in Tsd. EUR', "@y{0,0.0}"),]
        title = f'"{commodity_group}" export value distribution in {cg_year} for the states of Germany.'
    else:
        TOOLTIPS = [('year', "@x"),('import value in Tsd. EUR', "@y{0,0.0}"),]
        title = f'"{commodity_group}" import value distribution in {cg_year} for the states of Germany.'

    
    p = figure(width=700, height=230, x_axis_label="years", title=f'"{commodity_group}" in {state} between {cg_year[0]} and {cg_year[1]} years', tools='hover,wheel_zoom,save', tooltips=TOOLTIPS)
    
    if select_output == 'Export Weight':
        p.yaxis.axis_label = "net mass in tons"
        p.line(x, y,  line_width=1, line_color="navy",legend_label=f'Export Volume') #f'{select_output}: Net mass'
        p.legend.label_text_color = "navy"
    elif select_output == 'Import Weight':
        p.yaxis.axis_label = "net mass in tons"
        p.line(x, y,  line_width=1, line_color="brown",legend_label=f'Import Volume')
        p.legend.label_text_color = "brown"
    elif select_output == 'Export Value':
        p.yaxis.axis_label = "value in Tsd. EUR"
        p.line(x, y,  line_width=1, line_color="navy",legend_label=f'Export Value')
        p.legend.label_text_color = "navy"
    else:
        p.yaxis.axis_label = "value in Tsd. EUR"
        p.line(x, y,  line_width=1, line_color="brown",legend_label=f'Import Value')
        p.legend.label_text_color = "brown"
    
    # Format the y-axis labels with commas
    p.yaxis.formatter = NumeralTickFormatter(format="0,0.0")
    p.title.text_font_size = '9pt'
    p.xaxis.axis_label_text_font_size = "9pt"
    p.yaxis.axis_label_text_font_size = "9pt"
    p.xaxis.axis_label_text_font_style = "italic"   
    p.legend.label_text_font = "times"
    p.legend.label_text_font_style = "italic"
    p.legend.location = "top_left"
    p.xaxis.ticker = FixedTicker(ticks=x)
    # https://docs.bokeh.org/en/latest/docs/reference/models/tickers.html
    
    # Change background color
    p.background_fill_color = '#FFECB3'
    #p.background_fill_alpha = 0.5  # You can also change the alpha (transparency)

    # ref: https://docs.bokeh.org/en/2.4.2/docs/user_guide/styling.html

    return p

create_cg_plot(commodity_group="Rohstoffe", state="Sachsen", select_output='Export Weight', cg_year=[2018,2022])
create_cg_plot(commodity_group="Rohstoffe", state="Sachsen", select_output='Export Value', cg_year=[2018,2022])
create_cg_plot(commodity_group="Rohstoffe", state="Sachsen", select_output='Import Weight', cg_year=[2018,2022])
create_cg_plot(commodity_group="Rohstoffe", state="Sachsen", select_output='Import Value', cg_year=[2018,2022])

list_of_cg = sorted(new_cg_df['Commodity Group'].unique())
select_cg_plot = pn.widgets.Select(name='Select Commodity Group:', options=list_of_cg, width=250)

list_of_states_cg = sorted(new_cg_df['State'].unique())
select_state_cg = pn.widgets.Select(name='State:', options=list_of_states_cg,width=100, value="Sachsen")

year_range_cg = pn.widgets.RangeSlider(name='Year Range', start=sorted(new_cg_df['Year'].unique())[0], end=sorted(new_cg_df['Year'].unique())[-1], value=(2018, 2022), step=1)

bound_cg_line = pn.bind(create_cg_plot, commodity_group=select_cg_plot, state=select_state_cg,  select_output='Export Weight', cg_year=year_range_cg)
bound_cg_line_exp_v = pn.bind(create_cg_plot, commodity_group=select_cg_plot, state=select_state_cg, select_output='Export Value', cg_year=year_range_cg)
bound_cg_line_imp_w = pn.bind(create_cg_plot, commodity_group=select_cg_plot, state=select_state_cg, select_output='Import Weight', cg_year=year_range_cg)
bound_cg_line_imp_v = pn.bind(create_cg_plot, commodity_group=select_cg_plot, state=select_state_cg, select_output='Import Value', cg_year=year_range_cg)



# In[ ]:


###############################################################################################################################
# DATA TABLE TAB AS TABULAR DATA #
###############################################################################################################################

editors = {}
for column in dt0_f.columns:
    column_editor = {
        'type': 'editable',  # Set the type to 'editable'
        'value': False,      # Set the default value to False
    }
    editors[column] = column_editor
    

panel_tabular_data= dt0_f.pipe(pn.widgets.Tabulator, pagination='local', page_size = 18, formatters={'Year': 'integer'}, width=1350, height=600, editors=editors) #sizing_mode='stretch_width',
#  #sizing_mode='stretch_width',

tabular_year_slider = pn.widgets.RangeSlider(name='Years:', start=sorted(dt0_f['Year'].unique())[0], end=sorted(dt0_f['Year'].unique())[-1], value=(2018, 2022), step=1, width=200)
panel_tabular_data.add_filter(tabular_year_slider, 'Year')

multi_select_state_tabular = pn.widgets.MultiSelect(options=sorted(dt0_f['State'].unique()), name='Select: State')
panel_tabular_data.add_filter(multi_select_state_tabular, 'State')

multi_select_tg_tabular = pn.widgets.MultiSelect(options=sorted(dt0_f['Trade Group'].unique()), name='Select: Trade Group')
panel_tabular_data.add_filter(multi_select_tg_tabular, 'Trade Group')

# Create a widget with a question mark tooltip
tooltip = pn.widgets.TooltipIcon(value="Use 'Ctrl' for multiple selection as well as deselect the selected options!")

filename, button = panel_tabular_data.download_menu(
    text_kwargs={'name': 'Enter filename', 'value': 'Filtered_Data.csv', 'width': 150},
    button_kwargs={'name': 'Download table'},
)


# In[ ]:


###############################################################################################################################
# CONNECTION MAP with Lasso Select and PolyDraw Tooltips #
###############################################################################################################################

import pyproj
import numpy as np
import panel as pn
import geoviews as gv
import holoviews as hv
import pandas as pd
from bokeh.models import HoverTool, LassoSelectTool, PolyDrawTool
from panel.widgets import Button
from bokeh.models import ColumnDataSource

gv.extension('bokeh')

def get_circle_path(start, end, samples=200):
    sx, sy = start
    ex, ey = end
    g = pyproj.Geod(ellps='WGS84')
    (az12, az21, dist) = g.inv(sx, sy, ex, ey)
    lonlats = g.npts(sx, sy, ex, ey, samples)
    return [(sx, sy)] + lonlats + [(ex, ey)]

def create_visualization(new_width=1200, new_height=800):
    leipzig = (12.3731, 51.3397)
    global cities
    cities = {
        'London': (-0.1278, 51.5074),
        'Paris': (2.3522, 48.8566),
        'Rome': (12.4964, 41.9028),
        'Madrid': (-3.7038, 40.4168),
        'Istanbul': (28.9784, 41.0082),
        'Moscow': (37.6173, 55.7558),
        'Tokyo': (139.6917, 35.6895),
        'Beijing': (116.4074, 39.9042),
        'Delhi': (77.2090, 28.6139),
        'Bangkok': (100.4931, 13.7563),
        'Berlin': (13.4050, 52.5200),
        'Amsterdam': (4.8952, 52.3702),
        'Brussels': (4.3517, 50.8503),
        'Vienna': (16.3738, 48.2082),
        'Athens': (23.7275, 37.9838),
        'Stockholm': (18.0686, 59.3293),
        'Copenhagen': (12.5683, 55.6761),
        'Oslo': (10.7522, 59.9139),
        'Helsinki': (24.9384, 60.1699),
        'Warsaw': (21.0122, 52.2297),
        'Budapest': (19.0402, 47.4979),
        'Prague': (14.4378, 50.0755),
        'Bratislava': (17.1077, 48.1486),
        'Lisbon': (-9.1393, 38.7223),
        'Dublin': (-6.2603, 53.3498),
        'Edinburgh': (-3.1883, 55.9533),
        'Stockholm': (18.0686, 59.3293),
        'Bucharest': (26.1025, 44.4268),
        'Sofia': (23.3219, 42.6977),
        'Belgrade': (20.4489, 44.7866),
        'Zagreb': (15.9819, 45.8150),
        'Ljubljana': (14.5058, 46.0569),
        'Tallinn': (24.7536, 59.4369),
        'Riga': (24.1052, 56.9496),
        'Vilnius': (25.2797, 54.6872),
        'Seoul': (126.9780, 37.5665),
        'Shanghai': (121.4737, 31.2304),
        'Hong Kong': (114.1694, 22.3193),
        'Taipei': (121.5654, 25.0320),
        'Singapore': (103.8198, 1.3521),
        'Kuala Lumpur': (101.6869, 3.1390),
        'Manila': (120.9842, 14.5995),
        'Jakarta': (106.8650, -6.2088),
        'Hanoi': (105.8342, 21.0285),
        'Ho Chi Minh City': (106.7009, 10.7769),
        'Bangalore': (77.5946, 12.9716),
        'Mumbai': (72.8777, 19.0760),
        'Kolkata': (88.3639, 22.5726),
        'Dhaka': (90.4125, 23.8103),
        'Kathmandu': (85.3240, 27.7172),
        'Colombo': (79.9585, 6.9271),
        'Yangon': (96.1548, 16.8661),
        'Phnom Penh': (104.8772, 11.5564),
        'Vientiane': (102.5663, 17.9757),
        'Kuala Terengganu': (103.1440, 5.3374),
        'Bandar Seri Begawan': (114.9481, 4.9419),
        'Ulaanbaatar': (106.9186, 47.9214),
        'Astana': (71.4304, 51.1605),
        'Pyongyang': (125.7547, 39.0392),
        'Kuala Lumpur': (101.6869, 3.1390), 
    }

    paths = []
    for city, coords in cities.items():
        paths.append(get_circle_path(leipzig, coords))

    tiles = gv.tile_sources.OSM()
    path = gv.EdgePaths(paths, kdims=['Longitude', 'Latitude']).opts(color='grey', line_width=1)
    points_data = [(lon, lat, name) for name, (lon, lat) in cities.items()]
    points = gv.Points(points_data, kdims=['Longitude', 'Latitude'], vdims=['Name'])

    hover = HoverTool()
    hover.tooltips = [("Name", "@Name"), ("Longitude", "@Longitude{0.0000}"), ("Latitude", "@Latitude{0.0000}")]
    lasso = LassoSelectTool()

    plot = tiles * path * points.opts(
        color='green', size=10,
        tools=[hover, lasso],
        hover_line_color='white', hover_fill_color='red', hover_alpha=1
    ).opts(width=new_width, height=new_height)

    return plot, points

def selected_info(index):
    selected_cities = [list(cities.keys())[i] for i in index]
    data = {'Cities': selected_cities}
    return pd.DataFrame(data)

def selected_polygon_info(data):
    if not data or 'xs' not in data or not data['xs'] or 'ys' not in data or not data['ys']:
        return pd.DataFrame(columns=['Longitude', 'Latitude'])
    return pd.DataFrame({'Longitude': data['xs'][0], 'Latitude': data['ys'][0]})


#poly_draw = gv.Polygons([], vdims=['xs', 'ys']).opts(line_width=2.5, color="blue", tools=[PolyDrawTool()])

def clear_polygons(event):
    # 1. Clear the PolyDraw stream data
    polygon_stream.data = {'xs': [], 'ys': []}
    polygon_stream.event()

    # 2. Update the source data of poly_draw
    #poly_draw.data = []

clear_button = Button(name="Clear Polygons")
clear_button.on_click(clear_polygons)


def dashboard():
    global polygon_stream
    plot, points = create_visualization(new_width=1200, new_height=800)

    # Explicitly create PolyDrawTool instance
    poly_draw_tool = PolyDrawTool()

    # Specify tools for the main plot, but not for the PolyDraw layer
    poly_draw = gv.Polygons([]).opts(
        line_width=1, 
        line_color="seagreen",
        fill_color="mediumturquoise",  # This sets the fill color of the drawn polygon
        fill_alpha=0.3,
    )

    combined = plot.opts(tools=[poly_draw_tool]) * poly_draw

    selection = hv.streams.Selection1D(source=points)
    polygon_stream = hv.streams.PolyDraw(source=poly_draw)
    
    #polygon_stream = hv.streams.PolyDraw(source=poly_draw, tool=poly_draw_tool)

    cities_table = hv.DynamicMap(lambda index: hv.Table(selected_info(index), 'Cities'), streams=[selection]).opts(title="Selected Cities")
    polygon_table = hv.DynamicMap(lambda data: hv.Table(selected_polygon_info(data), ['Longitude', 'Latitude']), streams=[polygon_stream]).opts(title="Selected Polygon Coordinates")

    map_column = pn.Column(pn.pane.Markdown("# Connection Map: Saxony - Import Product Groups"), combined)
    table_column = pn.Column(cities_table, clear_button, polygon_table)

    return pn.Row(map_column, table_column)

connection_map = dashboard()


# In[ ]:


###############################################################################################################################
# CoyPu API Tab #
# Sparql Query Visualization #
###############################################################################################################################

import pandas as pd
import panel as pn
import requests

# Ensure Panel extension is loaded for notebook (if using Jupyter)
pn.extension('tabulator')

# Replace with your credentials
user = ""  
password = ""

# Function to execute the query and return results
def execute_query(event=None):
    query = text_input.value
    response = requests.post(
        f"https://{user}:{password}@skynet.coypu.org/coypu-internal/",
        data={'query': query}
    ).json().get("results", {}).get("bindings", [])

    all_keys = set()
    for item in response:
        all_keys.update(item.keys())

    # Create a dictionary to store the response
    data_dict = {key: [] for key in all_keys}

    # Populate the dictionary with values from the response
    for item in response:
        for key in all_keys:
            if key in item:
                data_dict[key].append(item[key]['value'])
            else:
                data_dict[key].append(None)

    # Create a DataFrame from the dictionary
    df = pd.DataFrame(data_dict)
    # print(df)
    
    # Update the Tabulator widget with new data
    panel_tabular_data_query.value = df

# Text input for the query with adjusted width
text_input = pn.widgets.TextAreaInput(name="Sparql Query Box", placeholder='Enter Your Query', width=800, height=200)  # Adjust the width as needed

# Button to execute the query
query_button = pn.widgets.Button(name='Execute Query', button_type='primary')
query_button.on_click(execute_query)

# Creating an empty Tabulator widget
panel_tabular_data_query = pn.widgets.Tabulator(
    pd.DataFrame(), 
    pagination='local', 
    page_size=18, 
    width=1350, 
    height=600
)

# Creating a Panel with a title, input, button, and the DataFrame widget
dashboard_query = pn.Column(
    pn.pane.Markdown("# Sparql Query Visualization"),
    text_input,
    query_button,
    panel_tabular_data_query
)


# In[ ]:


###############################################################################################################################
# DASHBOARD PANEL LAYOUT CUSTOMIZATION #
###############################################################################################################################

# Load the 'plotly' extension
pn.extension('plotly')

# Load the 'tabulator' extension
pn.extension('tabulator')

custom_css = """
.bk.bk-tabs {
    display: table;
    width: 100%;
    background-color: #EBC7C7FF;  /* Background color for the entire tab bar */
    border-collapse: collapse;
}

.bk.bk-tab {
    display: table-cell;
    padding: 7px 14px;  /* Adjust padding as needed */
    border: 1px solid #ccc;
    background-color: #C7EBE0FF;
    cursor: pointer;
    text-align: center;
}

.bk.bk-tabs .bk-active.bk-tab {
    background-color: green;  /* Active tab background color */
    color: white;  /* Active tab text color */
}

.bk.bk-tab:hover {
    background-color: #ddd;  /* Hover color for the tabs */
}
"""
# REFERENCE: https://discourse.holoviz.org/t/panel-is-it-possible-to-customize-the-background-color-text-colors-of-the-active-and-inactive-tabs-tags/781/2
# https://fffuel.co/cccolor/
# https://encycolorpedia.com/f2f2f2

# tab color = #ffc #61D4B1
# background-color: #f2f2f2;  /* Background color for the entire tab bar */

pn.extension(raw_css=[custom_css])


# TAB 1 : Visualization for Product Groups
# TAB 1.1 : Product Groups in total imports/exports
row_layout_1 = pn.Row(
    pn.Column(pn.Row(select_state, select_year, select_export_import), panel_layout_sankey_chart))
'''
REMOVED FROM row_layout_1
, 
pn.Column(pn.Row(select_trade_group_plot, select_state_plot, year_range_slider),
        pn.Spacer(height=5), bound_plot, 
        pn.Spacer(height=15), bound_plot_imp_w, 
        pn.Spacer(height=15), bound_plot_exp_v, 
        pn.Spacer(height=15), bound_plot_imp_v)
'''

# TAB 1 : Visualization for Product Groups
# TAB 1.2 : Imports/exports by product group and country of origin
row_layout_2 = pn.Row(
    pn.Column(pn.Row(select_trade_group,select_state_tg,select_year_tg,select_export_import_tg),panel_layout_sankey_chart_2), )


# TAB 1 : Visualization for Product Groups
# TAB 1.3 : Imports/exports by product group, country of origin and years
row_layout_2_line_plots = pn.Row(
                            pn.Column(pn.Row(select_trade_groups_c, select_states_c, select_country, year_range_slider_c), 
                                      pn.Spacer(height=15),bound_plot_c_imp_v,
                                      pn.Spacer(height=15), bound_plot_c_imp_w, 

                                      ), 
                            pn.Spacer(width=60),
                            pn.Column(pn.Spacer(height=75), bound_plot_c_exp_v, 
                                      pn.Spacer(height=5), bound_plot_c
                                      )
)

# TAB 1 : Visualization for Product Groups
# TAB 1.4 : Trade Volume & Value Distribution over States (BAR CHARTS)
row_layout_3 = pn.Column(
    pn.Row(select_trade_groups_bar, select_year_bar), 
    pn.Row(
        pn.Column(bound_bar_plot_tg, pn.Spacer(height=20), bound_bar_plot_tg_imp_w),
        pn.Spacer(width=20),
        pn.Column(bound_bar_plot_tg_exp_v, pn.Spacer(height=20), bound_bar_plot_tg_imp_v)))


# TAB 1 : Visualization for Product Groups
# TAB 1.5 : Share of product groups in total imports/exports (TREE MAPS)
row_layout_4 = pn.Column(
    pn.Row(select_states_tree,select_treemap_output, year_range_s), 
    bound_treemap_plot, bound_treemap_plot_pg)


# TAB 1 : Visualization for Product Groups
# TAB 1.6 : Dynamic Data Table
row_layout_6 = pn.Column(
    pn.Row(tabular_year_slider,multi_select_state_tabular, multi_select_tg_tabular,tooltip, pn.Column(filename, button)), 
    panel_tabular_data)


# TAB 1 : Visualization for Product Groups
# TAB 1.7 : Imports/exports by commodity group (Line Plots)
row_layout_7 = pn.Column(
    pn.Row(select_cg_plot, select_state_cg, year_range_cg), 
    pn.Row(
        pn.Column(bound_cg_line, pn.Spacer(height=20), bound_cg_line_exp_v),
        pn.Spacer(width=20),
        pn.Column(bound_cg_line_imp_w, pn.Spacer(height=20), bound_cg_line_imp_v)))

# TAB 1 : Visualization for Product Groups
# TAB 1.8 : Choropleth State Maps
row_layout_8 = pn.Column(select_states_choropleth, bound_choropleth_plot)


# TAB 2 : Effects of supply chain shocks on the economy in Saxony (CONNECTION MAP)
row_layout_5 = connection_map

# TAB 3 : CoyPu API
row_layout_9 = dashboard_query


# TAB 1 : GROUP DIFFERENT TABS UNDER A SINGLE TAB
tab_group1 = pn.layout.Tabs(
    ("Product Groups in total imports/exports", row_layout_1),
    ("Imports/exports by product group and country of origin", row_layout_2), 
    ("Imports/exports by product group, country of origin and years", row_layout_2_line_plots),
    ("Trade Volume & Value Distribution over States", row_layout_3),
    ("Share of product & commodity groups in total imports/exports", row_layout_4), 
    ("Data Table", row_layout_6),
    ("Imports/exports by commodity group", row_layout_7),
    ("Choropleth Map", row_layout_8),
) 


# FINAL TABS LOOK
group_tabs= pn.layout.Tabs(
    ("Visualization for Product Groups", tab_group1),
    ("Effects of supply chain shocks on the economy in Saxony", row_layout_5),
    ("CoyPu API", row_layout_9)
)

# Include the tabs in your main layout
app_layout = pn.Column(group_tabs)

# Serve Dashboard
app_layout.servable(title='CoyPu Public Dashboard: Demo')
pn.config.theme = 'light'


'''
# EXAMPLE
tabgroup1 = pn.layout.Tabs(
    ("Trade Groups per State", row_layout_1),
    ("Export/Import Countries per State", row_layout_2), 
    ("Trade Volume & Value Distribution over States", row_layout_3)
) # background='#F9F3C3'



tabgroup2= pn.layout.Tabs(
    ("Tree Map", row_layout_4),
    ("Choropleth Map", row_layout_5), 
    ("Table", row_layout_6),
    ("Line Plots: Commodity Groups", row_layout_7), dynamic=True, styles={'background': '#E0F7FA'}
)

tabs= pn.layout.Tabs(
    ("Group of Tabs 1", tabgroup1),
    ("Group of Tabs 2", tabgroup2)
)

'''


'''
    ("Line Plots: Commodity Groups", row_layout_7), dynamic=True, styles={'background': '#E0F7FA'}
'''


'''
tabs = pn.layout.Tabs(
    ("Trade Groups per State", row_layout_1),
    ("Export/Import Countries per State", row_layout_2), 
    ("Trade Volume & Value Distribution over States", row_layout_3), 
    ("Tree Map", row_layout_4),
    ("Choropleth Map", row_layout_5), 
    ("Table", row_layout_6), 
    dynamic=True, styles={'background': '#E0F7FA'}
) # background='#F9F3C3'
'''


# In[ ]:


###############################################################################################################################
# How to save panel dashboard as html file #
###############################################################################################################################

# Example 1
#app_layout.save('demo_dashboard_v13.html', embed=True)



# Example 2
#app_layout.save('test_dashboard_9.html', embed=True)



# In[1]:


get_ipython().system('pip list')


# In[ ]:




