# CoyPu Public Dashboard 
# Version 17 - 08.02.2024
# Includes: All available plots 
# Task 1: Polygon area is drawn on the map depending on the polygon area coordinate 
# Task 2: Dynamic menu creation and use selected event in query dynamically depending on the Event type selection on dynamic menu. Example: Disaster > Natural Disaster > GeophysicalDisaster > Earthquake etc.
# Task 3: Binding multiple queries' output with plots, map, etc. to have interactive and dynamic changes. 
#              Example: Event type - Flood > Send query using Flood to list all flood events > 
#                       Display events on table > Select an event on table and query is sent depending on this selection >
#                       Get polygon area of the selected event > Use this polygon area in query to get effected countries and trade groups >
#
# Tasks given above in progress are partly done:
# Task 1 : completed, will be integrated to main code
# Task 2 : in progress
# Task 3 : partly completed, will be integrated to main code


##########################################################################
To serve the dashboard, run the command: 
$ panel serve --port 5006 .\dashboard_v17.ipynb  --autoreload

On the browser:
http://localhost:5006/dashboard_v17 



# NOTE:
Visualisations depending on local data, and the their folder path should be changed for your pc before running the code. There are two places, the file paths are given for the data and maps:
1)
csv_file_names = [
    r'C:\Users\FatihKilic\Desktop\jupyter_notebooks\data_all\51000-0034-DLANDX_$F_flat.csv',
    r'C:\Users\FatihKilic\Desktop\jupyter_notebooks\data_all\51000-0034_flat_2008_2022.csv',
    r'C:\Users\FatihKilic\Desktop\jupyter_notebooks\data_all\51000-0036_flat_2018.csv',
...
...
]

2)
def create_choropleth_map(state_name):
    # Load GeoJSON data for Saxony districts using GeoPandas
    
    if state_name == "Baden-Württemberg":
        districts_gdf = gpd.read_file(r'C:\Users\FatihKilic\Desktop\jupyter_notebooks\panel\GeoDataGermany\geodataBadenW.geojson')
    elif state_name == "Bayern":
        districts_gdf = gpd.read_file(r'C:\Users\FatihKilic\Desktop\jupyter_notebooks\panel\GeoDataGermany\geodataBaveria.geojson')
    elif state_name == "Berlin":
        districts_gdf = gpd.read_file(r'C:\Users\FatihKilic\Desktop\jupyter_notebooks\panel\GeoDataGermany\geodataBerlin.json')
    elif state_name == "Brandenburg":
...
...
    else:
        print("State name is not in the state_name list.")
        None

