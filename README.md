# Public Dashboard



This was the first repository for the public dashboard and is now used for the scripts and data.

This repository mostly contains the transformation scripts used to prepare the data for the dashboard and the data itself (original and tranfsormed).

The repository for the newest version of the dashboard can be found [here](https://gitlab.com/coypu-project/dashboard) and comes without all the data.

# Datasets

Datasets used from [Genesis](https://www-genesis.destatis.de/genesis/online):

- [51000-0034 - Außenhandel](https://www-genesis.destatis.de/genesis//online?operation=table&code=51000-0034&bypass=true&levelindex=0&levelid=1705582732542#abreadcrumb)
- [51000-0036 - Außenhandel](https://www-genesis.destatis.de/genesis//online?operation=table&code=51000-0036&bypass=true&levelindex=0&levelid=1705582771626#abreadcrumb)
- [81000-0009 - VGR](https://www-genesis.destatis.de/genesis//online?operation=table&code=81000-0009&bypass=true&levelindex=0&levelid=1705582814746#abreadcrumb)
- [81000-0015 - VGR](https://www-genesis.destatis.de/genesis//online?operation=table&code=81000-0015&bypass=true&levelindex=0&levelid=1705582859174#abreadcrumb)
- [81000-0110 - VGR](https://www-genesis.destatis.de/genesis//online?operation=table&code=81000-0110&bypass=true&levelindex=0&levelid=1705582910184#abreadcrumb)
- [81000-0127 - VGR](https://www-genesis.destatis.de/genesis//online?operation=table&code=81000-0127&bypass=true&levelindex=0&levelid=1705582933775#abreadcrumb)
- [81000-0130 - VGR](https://www-genesis.destatis.de/genesis//online?operation=table&code=81000-0130&bypass=true&levelindex=0&levelid=1705582959482#abreadcrumb)
- [82111-0001 - VGR](https://www-genesis.destatis.de/genesis//online?operation=table&code=82111-0001&bypass=true&levelindex=0&levelid=1705582973687#abreadcrumb)
- [82111-0002 - VGR](https://www-genesis.destatis.de/genesis//online?operation=table&code=82111-0002&bypass=true&levelindex=0&levelid=1705583007261#abreadcrumb)
- [82411-0001 - VGR](https://www-genesis.destatis.de/genesis//online?operation=table&code=82411-0001&bypass=true&levelindex=0&levelid=1705583037631#abreadcrumb)


Other Datasets:

- [Input-Output-Rechnung](https://www.destatis.de/DE/Themen/Wirtschaft/Volkswirtschaftliche-Gesamtrechnungen-Inlandsprodukt/Publikationen/Downloads-Input-Output-Rechnung/input-output-rechnung-2180200207005.html)